class Solution {
    func findComplement(_ num: Int) -> Int {
        if num == 0 {
            return 0;
        }

        if num & 1 != 0 {
            return findComplement(num >> 1) << 1;
        }

        return (findComplement(num >> 1) << 1) + 1;
    }
}
