class Solution {
    func transpose(_ matrix: [[Int]]) -> [[Int]] {
        var m = Array(
            repeating: Array(repeating: 0, count: matrix.count),
            count: matrix[0].count
        )

        for y in 0..<matrix.count {
            for x in 0..<matrix[y].count {
                m[x][y] = matrix[y][x]
            }
        }

        return m
    }
}
