class Solution {
    func numSpecial(_ mat: [[Int]]) -> Int {
        let m = mat.count
        let n = mat[0].count

        var rows: [Int] = Array(repeating: 0, count: m)
        var cols: [Int] = Array(repeating: 0, count: n)

        for (y, x) in (0..<m).flatMap { y in (0..<n).map {x in (y, x) } } {
            if mat[y][x] == 1 {
                rows[y] += 1
                cols[x] += 1
            }
        }

        var count = 0
        for (y, x) in (0..<m).flatMap { y in (0..<n).map {x in (y, x) } } {
            if mat[y][x] == 1 && rows[y] == 1 && cols[x] == 1 {
                count += 1
            }
        }

        return count
    }
}
