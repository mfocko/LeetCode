class Solution {
    func findSpecialInteger(_ arr: [Int]) -> Int {
        let threshold = arr.count / 4

        var last = -1
        var counter = 0
        for x in arr {
            if last != x {
                counter = 1
                last = x
            } else {
                counter += 1
            }

            if counter > threshold {
                return last
            }
        }

        return -1
    }
}
