class StockSpanner {
    private var st: [(Int, Int)] = []

    init() {}

    func next(_ price: Int) -> Int {
        var span = 1
        while !st.isEmpty && st.last!.0 <= price {
            span += st.last!.1
            st.removeLast()
        }

        st.append((price, span))

        return span
    }
}

/**
 * Your StockSpanner object will be instantiated and called as such:
 * let obj = StockSpanner()
 * let ret_1: Int = obj.next(price)
 */
