class Solution {
    func containsDuplicate(_ nums: [Int]) -> Bool {
        var encountered = Set<Int>()
        for x in nums {
            if encountered.contains(x) {
                return true
            }
            encountered.insert(x)
        }
        return false
    }
}
