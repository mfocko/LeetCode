class Solution {
    func largestAltitude(_ gain: [Int]) -> Int {
        var m = 0

        var alt = 0
        for d in gain {
            alt += d
            m = max(m, alt)
        }

        return m
    }
}
