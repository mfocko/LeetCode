class Solution {
    func maxProduct(_ nums: [Int]) -> Int {
        var m = 0

        for i in 0..<nums.count - 1 {
            for j in i + 1..<nums.count {
                let p = (nums[i] - 1) * (nums[j] - 1)
                if p > m {
                    m = p
                }
            }
        }

        return m
    }
}
