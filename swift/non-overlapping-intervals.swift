class Solution {
    func eraseOverlapIntervals(_ intervals: [[Int]]) -> Int {
        let intervals = intervals
            .map { ($0[0], $0[1]) }
            .sorted { l, r in l.1 < r.1 }

        var lastEnd: Int = .min
        var erased = 0
        for (start, end) in intervals {
            if start >= lastEnd {
                lastEnd = end
            } else {
                erased += 1
            }
        }

        return erased
    }
}
