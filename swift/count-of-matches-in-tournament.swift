class Solution {
    func numberOfMatches(_ n: Int) -> Int {
        var n = n

        var matches = 0
        while n > 1 {
            if n % 2 == 0 {
                matches += n >> 1;
            } else {
                matches += n >> 1;
                n += 2;
            }

            n >>= 1;
        }

        return matches
    }
}
