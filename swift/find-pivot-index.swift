class Solution {
    func pivotIndex(_ nums: [Int]) -> Int {
        var fromLeft = 0
        var fromRight = nums.reduce(0, +)

        for (i, x) in nums.enumerated() {
            fromRight -= x

            if fromLeft == fromRight {
                return i
            }

            fromLeft += x
        }

        return -1
    }
}
