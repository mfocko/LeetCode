class Solution {
    func findMinArrowShots(_ points: [[Int]]) -> Int {
        let points = points.sorted(by: { a, b in return a[0] < b[0] })

        var usedArrows = 1
        var maxX = points[0][1]

        for point in points.dropFirst() {
            if point[0] <= maxX {
                maxX = min(maxX, point[1])
            } else {
                maxX = point[1]
                usedArrows += 1
            }
        }

        return usedArrows
    }
}
