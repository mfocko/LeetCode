class Solution {
    func twoSum(_ nums: [Int], _ target: Int) -> [Int] {
        var encountered: [Int: Int] = [:]

        for (i, x) in nums.enumerated() {
            if encountered[target - x] != nil {
                return [encountered[target - x]!, i]
            }

            encountered[x] = i
        }

        return []
    }
}
