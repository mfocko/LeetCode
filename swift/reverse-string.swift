class Solution {
    func reverseString(_ s: inout [Character]) {
        for i in 0..<(s.count / 2) {
            let j = s.count - i - 1
            (s[i], s[j]) = (s[j], s[i])
        }
    }
}
