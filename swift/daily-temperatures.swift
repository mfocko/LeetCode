class Solution {
    func dailyTemperatures(_ temperatures: [Int]) -> [Int] {
        var result = [Int](repeating: 0, count: temperatures.count)
        var st: [Int] = []

        for (i, t) in temperatures.enumerated() {
            while !st.isEmpty && temperatures[st.last!] < t {
                result[st.last!] = i - st.last!
                st.removeLast()
            }

            st.append(i)
        }

        return result
    }
}
