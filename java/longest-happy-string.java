class Solution {
  private char tryAppend(int[] allowed, int[] trailing, char letter) {
    int i = letter - 'a';

    if ((allowed[i] >= allowed[(i + 1) % 3]
            && allowed[i] >= allowed[(i + 2) % 3]
            && trailing[i] != 2)
        || (allowed[i] > 0 && (trailing[(i + 1) % 3] == 2 || trailing[(i + 2) % 3] == 2))) {
      --allowed[i];
      ++trailing[i];
      trailing[(i + 1) % 3] = 0;
      trailing[(i + 2) % 3] = 0;
      return letter;
    }

    return '\0';
  }

  public String longestDiverseString(int a, int b, int c) {
    var longest = new StringBuilder();

    var allowed = new int[] {a, b, c};
    var trailing = new int[3];
    for (int i = a + b + c; i > 0; --i) {
      for (var letter : new char[] {'a', 'b', 'c'}) {
        var toAppend = tryAppend(allowed, trailing, letter);
        if (toAppend != '\0') {
          longest.append(letter);
          break;
        }
      }
    }

    return longest.toString();
  }
}
