class Solution {
  private boolean matches(ListNode head, TreeNode node) {
    // linked list ran out ⇒ matches
    if (head == null) {
      return true;
    }

    // ran out of tree nodes ⇒ failed
    if (node == null) {
      return false;
    }

    // values don't match ⇒ failed
    if (head.val != node.val) {
      return false;
    }

    // check rest of the path
    return matches(head.next, node.left) || matches(head.next, node.right);
  }

  public boolean isSubPath(ListNode head, TreeNode node) {
    // cannot form a subpath
    if (node == null) {
      return false;
    }

    // check path starting from the current node
    var result = matches(head, node);

    // if doesn't match linked list, try from subtrees
    result = result || isSubPath(head, node.left);
    result = result || isSubPath(head, node.right);

    return result;
  }
}
