class Solution {
  public int minSwaps(String s) {
    var open = 0;
    for (var c : s.toCharArray()) {
      switch (c) {
        case '[':
          ++open;
          break;
        case ']':
          if (open > 0) {
            --open;
          }
          break;
        default:
          /* no-op */
          break;
      }
    }

    return (1 + open) / 2;
  }
}
