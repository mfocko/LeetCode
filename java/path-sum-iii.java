class Solution {
  private int pathSumIncluding(TreeNode node, long target) {
    if (node == null) {
      return 0;
    }

    return (node.val == target ? 1 : 0)
        + pathSumIncluding(node.left, target - node.val)
        + pathSumIncluding(node.right, target - node.val);
  }

  public int pathSum(TreeNode node, int targetSum) {
    if (node == null) {
      return 0;
    }

    return pathSumIncluding(node, targetSum)
        + pathSum(node.left, targetSum)
        + pathSum(node.right, targetSum);
  }
}
