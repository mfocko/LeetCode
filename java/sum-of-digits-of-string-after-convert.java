class Solution {
  public int getLucky(String s, int k) {
    // First iteration
    int lucky = 0;
    for (var c : s.toCharArray()) {
      var number = c - 'a' + 1;
      while (number > 0) {
        lucky += number % 10;
        number /= 10;
      }
    }

    for (int i = 1; i < k; ++i) {
      var nextLucky = 0;
      for (; lucky > 0; lucky /= 10) {
        nextLucky += lucky % 10;
      }
      lucky = nextLucky;
    }

    return lucky;
  }
}
