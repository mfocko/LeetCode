public class TreeNode {
  int val;
  TreeNode left;
  TreeNode right;

  TreeNode() {}

  TreeNode(int val) {
    this.val = val;
  }

  TreeNode(int val, TreeNode left, TreeNode right) {
    this.val = val;
    this.left = left;
    this.right = right;
  }
}

class Solution {
  public boolean evaluateTree(TreeNode node) {
    if (node == null) {
      throw new NullPointerException("node cannot be null");
    }

    return switch (node.val) {
      case 0 -> false;
      case 1 -> true;
      case 2 -> evaluateTree(node.left) || evaluateTree(node.right);
      case 3 -> evaluateTree(node.left) && evaluateTree(node.right);
      default -> throw new IllegalStateException("value must be in range ⟨0;3⟩");
    };
  }
}
