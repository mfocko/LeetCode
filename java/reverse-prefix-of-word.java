class Solution {
  public String reversePrefix(String word, char ch) {
    var sb = new StringBuilder(word.length());

    var canSwap = true;
    for (var i = 0; i < word.length(); ++i) {
      char c = word.charAt(i);
      sb.append(c);

      if (c == ch && canSwap) {
        sb.reverse();
        canSwap = false;
      }
    }

    return sb.toString();
  }
}
