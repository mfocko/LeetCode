public class TreeNode {
  int val;
  TreeNode left;
  TreeNode right;

  TreeNode() {}

  TreeNode(int val) {
    this.val = val;
  }

  TreeNode(int val, TreeNode left, TreeNode right) {
    this.val = val;
    this.left = left;
    this.right = right;
  }
}

class Solution {
  private int distribute(TreeNode node) {
    if (node == null) {
      return 0;
    }

    int left = distribute(node.left);
    int right = distribute(node.right);

    moves += Math.abs(left) + Math.abs(right);

    return node.val - 1 + left + right;
  }

  private int moves;

  public int distributeCoins(TreeNode root) {
    moves = 0;
    distribute(root);
    return moves;
  }
}
