class Solution {
  public int maxArea(int[] height) {
    int foundMax = 0;

    int l = 0, r = height.length - 1;
    while (l < r) {
      foundMax = Math.max(foundMax, (r - l) * Math.min(height[l], height[r]));

      if (height[l] < height[r]) {
        ++l;
      } else {
        --r;
      }
    }

    return foundMax;
  }
}
