class Solution {
  private static final int END = 0;
  private static final int START = 1;

  private record Event(int time, int type, int payoff) {}

  public int maxTwoEvents(int[][] events) {
    var times = new ArrayList<Event>();
    for (var event : events) {
      times.add(new Event(event[0], START, event[2]));
      times.add(new Event(event[1] + 1, END, event[2]));
    }
    times.sort(Comparator.comparing(Event::time).thenComparing(Event::type));

    int answer = 0, maxPayoff = 0;
    for (var time : times) {
      switch (time.type) {
        case START:
          answer = Math.max(answer, time.payoff + maxPayoff);
          break;
        case END:
          maxPayoff = Math.max(maxPayoff, time.payoff);
          break;
      }
    }

    return answer;
  }
}
