class Solution {
  public boolean makeEqual(String[] words) {
    int[] counters = new int[26];

    for (String w : words) {
      for (int i = 0; i < w.length(); ++i) {
        ++counters[w.charAt(i) - 'a'];
      }
    }

    int N = words.length;
    for (int count : counters) {
      if (count % N != 0) {
        return false;
      }
    }

    return true;
  }
}
