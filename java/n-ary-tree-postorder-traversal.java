import java.util.ArrayList;
import java.util.List;

class Solution {
  private ArrayList<Integer> postorder(Node node, ArrayList<Integer> l) {
    if (node == null) {
      return l;
    }

    for (var child : node.children) {
      postorder(child, l);
    }
    l.add(node.val);

    return l;
  }

  public List<Integer> postorder(Node root) {
    return postorder(root, new ArrayList<Integer>());
  }
}
