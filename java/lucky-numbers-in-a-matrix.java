class Solution {
  public List<Integer> luckyNumbers(int[][] matrix) {
    int rows = matrix.length, cols = matrix[0].length;

    int rowCandidate = Integer.MIN_VALUE;
    for (int y = 0; y < rows; ++y) {
      int currentRow = matrix[y][0];
      for (int x = 1; x < cols; ++x) {
        currentRow = Math.min(currentRow, matrix[y][x]);
      }
      rowCandidate = Math.max(rowCandidate, currentRow);
    }

    int colCandidate = Integer.MAX_VALUE;
    for (int x = 0; x < cols; ++x) {
      int currentCol = matrix[0][x];
      for (int y = 1; y < rows; ++y) {
        currentCol = Math.max(currentCol, matrix[y][x]);
      }
      colCandidate = Math.min(colCandidate, currentCol);
    }

    if (rowCandidate == colCandidate) {
      return Arrays.asList(rowCandidate);
    }

    return List.<Integer>of();
  }
}
