class Solution {
  public String addSpaces(String s, int[] spaces) {
    var result = new StringBuilder();
    result.ensureCapacity(s.length() + spaces.length);

    int i = 0;
    for (int j = 0; j < s.length(); ++j) {
      if (i < spaces.length && spaces[i] == j) {
        result.append(' ');
        ++i;
      }

      result.append(s.charAt(j));
    }

    return result.toString();
  }
}
