class Solution {
  public int longestSubarray(int[] nums) {
    int maximum = 0;

    int longest = 0, current = 0;
    for (var num : nums) {
      if (maximum < num) {
        maximum = num;
        longest = current = 1;

        continue;
      }

      if (num == maximum) {
        ++current;
      } else {
        current = 0;
      }

      longest = Math.max(longest, current);
    }

    return longest;
  }
}
