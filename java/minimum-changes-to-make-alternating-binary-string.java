class Solution {
  public int minOperations(String s) {
    int startingZero = 0;
    int startingOne = 0;

    for (int i = 0; i < s.length(); ++i) {
      char c = s.charAt(i);

      if (c != '0' + i % 2) {
        ++startingZero;
      }

      if (c != '0' + (i + 1) % 2) {
        ++startingOne;
      }
    }

    return Math.min(startingZero, startingOne);
  }
}
