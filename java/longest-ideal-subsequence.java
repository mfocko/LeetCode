class Solution {
  public int longestIdealString(String s, int k) {
    int[] lengths = new int[26];

    int longest = 0;
    for (int i = 0; i < s.length(); ++i) {
      int c = s.charAt(i) - 'a';

      int longestIncluding = 0;
      for (int pred = 0; pred < 26; ++pred) {
        if (Math.abs(pred - c) > k) {
          continue;
        }

        longestIncluding = Math.max(longestIncluding, lengths[pred]);
      }

      lengths[c] = Math.max(lengths[c], longestIncluding + 1);
      longest = Math.max(longest, lengths[c]);
    }

    return longest;
  }
}
