class Solution {
  public int isPrefixOfWord(String sentence, String searchWord) {
    var words = sentence.split(" ");
    for (int i = 0; i < words.length; ++i) {
      if (words[i].startsWith(searchWord)) {
        return 1 + i;
      }
    }

    return -1;
  }
}
