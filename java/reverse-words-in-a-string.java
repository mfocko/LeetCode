class Solution {
  private static <T> void reverse(T[] arr) {
    for (int l = 0, r = arr.length - 1; l < r; ++l, --r) {
      var tmp = arr[l];
      arr[l] = arr[r];
      arr[r] = tmp;
    }
  }

  public String reverseWords(String s) {
    String[] words = s.strip().split("\\s+");
    reverse(words);

    return String.join(" ", words);
  }
}
