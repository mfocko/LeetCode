class Solution {
  public TreeNode lowestCommonAncestor(TreeNode node, TreeNode p, TreeNode q) {
    if (node == null) {
      return null;
    }

    if (node.val == p.val || node.val == q.val) {
      return node;
    }

    var left = lowestCommonAncestor(node.left, p, q);
    var right = lowestCommonAncestor(node.right, p, q);

    if (left == null) {
      return right;
    }

    if (right == null) {
      return left;
    }

    return node;
  }
}
