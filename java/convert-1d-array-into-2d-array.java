class Solution {
  public int[][] construct2DArray(int[] original, int m, int n) {
    if (original.length != m * n) {
      return new int[0][0];
    }

    var array = new int[m][n];
    for (int i = 0; i < m * n; ++i) {
      int y = i / n, x = i % n;
      array[y][x] = original[i];
    }

    return array;
  }
}
