class Solution {
  public int minAddToMakeValid(String s) {
    int open = 0, needed = 0;

    for (var c : s.toCharArray()) {
      switch (c) {
        case '(':
          ++open;
          break;
        case ')':
          if (open > 0) {
            --open;
          } else {
            ++needed;
          }
          break;
        default:
          /* no-op */
          break;
      }
    }

    return open + needed;
  }
}
