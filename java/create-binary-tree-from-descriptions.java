class Solution {
  public TreeNode createBinaryTree(int[][] descriptions) {
    Map<Integer, TreeNode> nodes = new HashMap<>();

    Set<Integer> hasParent = new HashSet<>();
    for (int[] description : descriptions) {
      int parent = description[0];
      int value = description[1];
      boolean isLeft = description[2] == 1;

      var parentNode = nodes.computeIfAbsent(parent, k -> new TreeNode(k));
      var node = nodes.computeIfAbsent(value, k -> new TreeNode(k));

      if (isLeft) {
        parentNode.left = node;
      } else {
        parentNode.right = node;
      }

      hasParent.add(value);
    }

    for (var node : nodes.values()) {
      if (!hasParent.contains(node.val)) {
        return node;
      }
    }

    return null;
  }
}
