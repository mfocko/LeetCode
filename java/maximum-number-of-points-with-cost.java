class Solution {
  public long maxPoints(int[][] points) {
    int width = points[0].length;

    var previous = new long[width];
    for (var row : points) {
      var current = new long[width];

      long runningMaximum = 0;
      for (int x = 0; x < width; ++x) {
        runningMaximum = Math.max(runningMaximum - 1, previous[x]);
        current[x] = runningMaximum;
      }

      runningMaximum = 0;
      for (int x = width - 1; x >= 0; --x) {
        runningMaximum = Math.max(runningMaximum - 1, previous[x]);
        current[x] = Math.max(current[x], runningMaximum) + row[x];
      }

      previous = current;
    }

    var answer = previous[0];
    for (var candidate : previous) {
      answer = Math.max(answer, candidate);
    }
    return answer;
  }
}
