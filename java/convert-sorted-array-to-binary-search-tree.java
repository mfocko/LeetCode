/**
 * Definition for a binary tree node. public class TreeNode { int val; TreeNode left; TreeNode
 * right; TreeNode() {} TreeNode(int val) { this.val = val; } TreeNode(int val, TreeNode left,
 * TreeNode right) { this.val = val; this.left = left; this.right = right; } }
 */
class Solution {
  private TreeNode sortedArrayToBST(int[] nums, int min, int max) {
    if (min > max) {
      return null;
    }

    int mid = (min + max) / 2;
    return new TreeNode(
        nums[mid], sortedArrayToBST(nums, min, mid - 1), sortedArrayToBST(nums, mid + 1, max));
  }

  public TreeNode sortedArrayToBST(int[] nums) {
    return sortedArrayToBST(nums, 0, nums.length - 1);
  }
}
