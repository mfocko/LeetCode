class Solution {
  public int minLength(String s) {
    var st = new Stack<Character>();

    for (var c : s.toCharArray()) {
      var last = !st.empty() ? st.peek() : '\0';
      if (last == 'A' && c == 'B') {
        st.pop();
      } else if (last == 'C' && c == 'D') {
        st.pop();
      } else {
        st.push(c);
      }
    }

    return st.size();
  }
}
