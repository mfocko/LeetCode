/**
 * Definition for singly-linked list. public class ListNode { int val; ListNode next; ListNode(int
 * x) { val = x; } }
 */
class Solution {
  private boolean isLastNode(ListNode node) {
    return node != null && node.next == null;
  }

  public void deleteNode(ListNode node) {
    while (node != null) {
      node.val = node.next.val;

      if (isLastNode(node.next)) {
        node.next = null;
      }

      node = node.next;
    }
  }
}
