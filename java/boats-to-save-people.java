import java.util.Arrays;

class Solution {
  public int numRescueBoats(int[] people, int limit) {
    Arrays.parallelSort(people);

    int counter = 0;

    int left = 0, right = people.length - 1;
    while (left <= right && left < people.length && right >= 0) {
      ++counter;

      // they both fit
      if (left < right && people[left] + people[right] <= limit) {
        ++left;
      }

      // the heaviest person is always included
      --right;
    }

    return counter;
  }
}
