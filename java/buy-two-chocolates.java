class Solution {
  public int buyChoco(int[] prices, int money) {
    int[] mins = new int[2];
    mins[0] = mins[1] = Integer.MAX_VALUE;

    for (int p : prices) {
      if (p < mins[0]) {
        mins[1] = mins[0];
        mins[0] = p;
      } else if (p < mins[1]) {
        mins[1] = p;
      }
    }

    int leftover = money - mins[0] - mins[1];
    if (leftover < 0) {
      return money;
    }
    return leftover;
  }
}
