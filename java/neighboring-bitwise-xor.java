class Solution {
  public boolean doesValidArrayExist(int[] derived) {
    var cumXor = 0;

    for (var x : derived) {
      cumXor ^= x;
    }

    return cumXor == 0;
  }
}
