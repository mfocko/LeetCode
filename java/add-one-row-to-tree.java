class Solution {
  private enum Side {
    Left,
    Right
  }

  private TreeNode addOneRow(int depth, int val, TreeNode node, int currentDepth, Side s) {
    if (depth == currentDepth) {
      var newRoot = new TreeNode(val);
      switch (s) {
        case Left:
          newRoot.left = node;
          break;
        case Right:
          newRoot.right = node;
          break;
      }

      return newRoot;
    }

    if (node == null) {
      return node;
    }

    node.left = addOneRow(depth, val, node.left, currentDepth + 1, Side.Left);
    node.right = addOneRow(depth, val, node.right, currentDepth + 1, Side.Right);

    return node;
  }

  public TreeNode addOneRow(TreeNode root, int val, int depth) {
    return addOneRow(depth, val, root, 1, Side.Left);
  }
}
