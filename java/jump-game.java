class Solution {
  public boolean canJump(int[] nums) {
    var last = 0;
    for (var i = 0; i < nums.length && last < nums.length - 1; ++i) {
      if (i > last) {
        // can't reach the current position
        return false;
      }

      last = Math.max(last, i + nums[i]);
    }

    return last >= nums.length - 1;
  }
}
