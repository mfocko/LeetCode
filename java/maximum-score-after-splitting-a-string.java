class Solution {
  public int maxScore(String s) {
    // count the ones
    int ones = 0;
    for (int i = 1; i < s.length(); ++i) {
      if (s.charAt(i) == '1') {
        ++ones;
      }
    }
    int zeros = s.charAt(0) == '0' ? 1 : 0;

    int foundScore = ones + zeros;
    for (int i = 1; i < s.length() - 1; ++i) {
      switch (s.charAt(i)) {
        case '0':
          ++zeros;
          break;
        case '1':
          --ones;
          break;
      }

      foundScore = Math.max(foundScore, ones + zeros);
    }

    return foundScore;
  }
}
