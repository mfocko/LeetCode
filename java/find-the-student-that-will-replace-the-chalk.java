class Solution {
  private long getSum(int[] chalk, int k) {
    long sum = 0;

    for (int uses : chalk) {
      sum += uses;
      if (sum > k) {
        break;
      }
    }

    return sum;
  }

  public int chalkReplacer(int[] chalk, int k) {
    var partialSum = getSum(chalk, k);

    // Remove whole iterations over the students
    k %= partialSum;

    for (int i = 0; i < chalk.length; ++i) {
      if (k < chalk[i]) {
        return i;
      }

      k -= chalk[i];
    }

    return 0;
  }
}
