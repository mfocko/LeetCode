class Solution {
  public int numTeams(int[] rating) {
    int[][][] dp = new int[2][3][rating.length];

    // base
    for (int i = 0; i < rating.length; ++i) {
      dp[0][0][i] = 1;
      dp[1][0][i] = 1;
    }

    for (int count = 2; count <= 3; ++count) {
      for (int i = 0; i < rating.length; ++i) {
        for (int j = i + 1; j < rating.length; ++j) {
          if (rating[i] < rating[j]) {
            dp[0][count - 1][j] += dp[0][count - 2][i];
          }

          if (rating[i] > rating[j]) {
            dp[1][count - 1][j] += dp[1][count - 2][i];
          }
        }
      }
    }

    int teams = 0;
    for (int i = 0; i < rating.length; ++i) {
      teams += dp[0][2][i] + dp[1][2][i];
    }

    return teams;
  }
}
