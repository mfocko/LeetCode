class Solution {
  public ListNode reverseList(ListNode head) {
    ListNode tail = null;

    while (head != null) {
      var tmp = head.next;
      head.next = tail;
      tail = head;
      head = tmp;
    }

    return tail;
  }
}
