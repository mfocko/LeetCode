import java.util.ArrayList;
import java.util.List;

class Solution {
  private ArrayList<Integer> postorderTraversal(TreeNode node, ArrayList<Integer> l) {
    if (node == null) {
      return l;
    }

    postorderTraversal(node.left, l);
    postorderTraversal(node.right, l);

    l.add(node.val);
    return l;
  }

  public List<Integer> postorderTraversal(TreeNode root) {
    return postorderTraversal(root, new ArrayList<Integer>());
  }
}
