class Solution {
  private boolean isValid(int n, int y, int x) {
    return y >= 0 && y < n && x >= 0 && x < n;
  }

  private void addLocal(int[][] grid, int[][] result, int y, int x) {
    for (int d = 0; d < 9; ++d) {
      int dy = (d / 3) - 1;
      int dx = (d % 3) - 1;

      if (!isValid(grid.length - 2, y - 1 + dy, x - 1 + dx)) {
        continue;
      }

      result[y - 1 + dy][x - 1 + dx] = Math.max(result[y - 1 + dy][x - 1 + dx], grid[y][x]);
    }
  }

  public int[][] largestLocal(int[][] grid) {
    int N = grid.length;
    int[][] result = new int[N - 2][N - 2];

    for (int i = 0; i < N * N; ++i) {
      int y = i / N;
      int x = i % N;

      addLocal(grid, result, y, x);
    }

    return result;
  }
}
