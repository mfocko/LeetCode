class Solution {
  public long minimumSteps(String s) {
    long swaps = 0;

    int blackBalls = 0;
    for (int i = 0; i < s.length(); ++i) {
      switch (s.charAt(i)) {
        case '0':
          swaps += blackBalls;
          break;
        case '1':
          ++blackBalls;
          break;
      }
    }

    return swaps;
  }
}
