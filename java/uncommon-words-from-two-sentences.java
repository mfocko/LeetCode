import java.util.ArrayList;
import java.util.HashMap;

class Solution {
  public String[] uncommonFromSentences(String s1, String s2) {
    var counters = new HashMap<String, Integer>();

    for (String word : s1.strip().split("\\s+")) {
      counters.compute(word, (key, count) -> 1 + (count != null ? count : 0));
    }

    for (String word : s2.strip().split("\\s+")) {
      counters.compute(word, (key, count) -> 1 + (count != null ? count : 0));
    }

    var uncommon = new ArrayList<String>();
    for (var entry : counters.entrySet()) {
      if (entry.getValue() != 1) {
        continue;
      }

      uncommon.add(entry.getKey());
    }

    return uncommon.toArray(new String[0]);
  }
}
