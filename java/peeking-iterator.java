class PeekingIterator implements Iterator<Integer> {
  private Iterator<Integer> it;

  private Integer pVal;
  private boolean pHasNext;

  public PeekingIterator(Iterator<Integer> iterator) {
    it = iterator;
    next();
  }

  public Integer peek() {
    return pVal;
  }

  @Override
  public Integer next() {
    pHasNext = it.hasNext();

    Integer oldValue = pVal;
    if (pHasNext) {
      pVal = it.next();
    }

    return oldValue;
  }

  @Override
  public boolean hasNext() {
    return pHasNext;
  }
}
