import java.util.HashSet;

class Solution {
  public boolean checkIfExist(int[] arr) {
    var seen = new HashSet<Integer>();
    for (var x : arr) {
      if (seen.contains(2 * x) || ((x & 1) == 0 && seen.contains(x / 2))) {
        return true;
      }

      seen.add(x);
    }

    return false;
  }
}
