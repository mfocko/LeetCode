class Solution {
  private long sumFirst(int[] nums, int k) {
    long sum = 0;
    for (var i = 0; i < k; ++i) {
      sum += nums[i];
    }
    return sum;
  }

  public double findMaxAverage(int[] nums, int k) {
    var runningSum = sumFirst(nums, k);
    var foundMax = runningSum;

    for (var i = k; i < nums.length; ++i) {
      runningSum += nums[i] - nums[i - k];
      foundMax = Math.max(foundMax, runningSum);
    }

    return (double) foundMax / k;
  }
}
