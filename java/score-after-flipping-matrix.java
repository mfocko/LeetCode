class Solution {
  public int matrixScore(int[][] grid) {
    int rows = grid.length;
    int cols = grid[0].length;

    int score = rows * (1 << (cols - 1));

    for (int x = 1; x < cols; ++x) {
      int same = 0;
      for (int y = 0; y < rows; ++y) {
        if (grid[y][x] == grid[y][0]) {
          ++same;
        }
      }

      same = Math.max(same, rows - same);
      score += same * (1 << (cols - x - 1));
    }

    return score;
  }
}
