class Solution {
  private boolean isLeaf(TreeNode node) {
    return node != null && node.left == null && node.right == null;
  }

  private int sumNumbers(TreeNode node, int number) {
    if (node == null) {
      return 0;
    }

    number = 10 * number + node.val;
    if (isLeaf(node)) {
      return number;
    }

    return sumNumbers(node.left, number) + sumNumbers(node.right, number);
  }

  public int sumNumbers(TreeNode root) {
    return sumNumbers(root, 0);
  }
}
