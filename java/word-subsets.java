class Solution {
  private static int[] getFreqs(String s) {
    var freqs = new int[26];

    for (var i = 0; i < s.length(); ++i) {
      ++freqs[s.charAt(i) - 'a'];
    }

    return freqs;
  }

  private static void merge(int[] cum, int[] freqs) {
    for (var i = 0; i < 26; ++i) {
      cum[i] = Math.max(cum[i], freqs[i]);
    }
  }

  private static boolean isUniversal(int[] freqs2, String w) {
    var freqs = getFreqs(w);

    for (var i = 0; i < 26; ++i) {
      if (freqs2[i] > freqs[i]) {
        return false;
      }
    }

    return true;
  }

  public List<String> wordSubsets(String[] words1, String[] words2) {
    var cumulativeFreqs = new int[26];
    for (var i = 0; i < words2.length; ++i) {
      var freqs = getFreqs(words2[i]);
      merge(cumulativeFreqs, freqs);
    }

    return Arrays.stream(words1).filter(w -> isUniversal(cumulativeFreqs, w)).toList();
  }
}
