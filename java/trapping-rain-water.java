class Solution {
  public int trap(int[] height) {
    var caught = 0;

    int i = 0, j = height.length - 1;
    int maxLeft = height[i], maxRight = height[j];
    while (i < j) {
      maxLeft = Math.max(maxLeft, height[i]);
      maxRight = Math.max(maxRight, height[j]);

      if (maxLeft < maxRight) {
        caught += maxLeft - height[i++];
      } else {
        caught += maxRight - height[j--];
      }
    }

    return caught;
  }
}
