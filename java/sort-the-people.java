import java.util.Comparator;

class Solution {
  private record Person(String name, int height) {}

  public String[] sortPeople(String[] names, int[] heights) {
    var people = new Person[names.length];
    for (int i = 0; i < names.length; ++i) {
      people[i] = new Person(names[i], heights[i]);
    }

    Arrays.sort(people, Comparator.comparing(p -> ((Person) p).height).reversed());

    return Arrays.stream(people).map(p -> p.name).toArray(String[]::new);
  }
}
