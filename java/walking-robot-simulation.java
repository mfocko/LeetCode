import java.util.HashSet;

class Solution {
  private static final int TURN_LEFT = -2;
  private static final int TURN_RIGHT = -1;

  private record Vec(int x, int y) {
    public Vec turnLeft() {
      return new Vec(-y, x);
    }

    public Vec turnRight() {
      return new Vec(y, -x);
    }

    public Vec add(Vec other) {
      return new Vec(x + other.x, y + other.y);
    }

    public int distance() {
      return Math.abs(x * x + y * y);
    }
  }

  public int robotSim(int[] commands, int[][] obstacles) {
    int maxDistance = 0;

    // Process obstacles
    var uniqueObstacles = new HashSet<Vec>();
    for (int[] obstacle : obstacles) {
      uniqueObstacles.add(new Vec(obstacle[0], obstacle[1]));
    }

    var pos = new Vec(0, 0);
    var direction = new Vec(0, 1);

    for (int command : commands) {
      if (command == TURN_LEFT) {
        direction = direction.turnLeft();
      } else if (command == TURN_RIGHT) {
        direction = direction.turnRight();
      }

      for (var steps = 0; steps < command; ++steps) {
        var next = pos.add(direction);
        if (uniqueObstacles.contains(next)) {
          break;
        }

        pos = next;
        maxDistance = Math.max(maxDistance, pos.distance());
      }
    }

    return maxDistance;
  }
}
