class Solution {
  public int countSeniors(String[] details) {
    int count = 0;

    for (String citizen : details) {
      var age = Integer.parseInt(citizen.substring(11, 13));

      if (age > 60) {
        ++count;
      }
    }

    return count;
  }
}
