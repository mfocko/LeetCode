class Solution {
  private static final int[] EVEN = new int[] {0, 1};

  public long maximumValueSum(int[] nums, int k, int[][] edges) {
    int n = nums.length;
    long[][] dp = new long[n + 1][2];

    dp[n][0] = Integer.MIN_VALUE;
    dp[n][1] = 0;

    for (int i = n - 1; i >= 0; --i) {
      for (int even : EVEN) {
        var xor = dp[i + 1][even ^ 1] + (nums[i] ^ k);
        var skip = dp[i + 1][even] + nums[i];

        dp[i][even] = Math.max(xor, skip);
      }
    }

    return dp[0][1];
  }
}
