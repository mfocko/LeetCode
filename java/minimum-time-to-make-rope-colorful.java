class Solution {
  public int minCost(String colors, int[] neededTime) {
    int[] dp = new int[colors.length() + 1];
    Arrays.fill(dp, 0);
    char previousColor = 0;
    int previousTime = 0;

    for (int i = 1; i <= colors.length(); ++i) {
      if (colors.charAt(i - 1) == previousColor) {
        dp[i] = dp[i - 1] + Math.min(previousTime, neededTime[i - 1]);
        previousTime = Math.max(previousTime, neededTime[i - 1]);
      } else {
        dp[i] = dp[i - 1];
        previousColor = colors.charAt(i - 1);
        previousTime = neededTime[i - 1];
      }
    }

    return dp[colors.length()];
  }
}
