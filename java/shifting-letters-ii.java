class Solution {
  public String shiftingLetters(String s, int[][] shifts) {
    var diff = new int[s.length()];
    for (var shift : shifts) {
      var start = shift[0];
      var end = shift[1];

      if (shift[2] == 0) {
        --diff[start];

        if (end + 1 < s.length()) {
          ++diff[end + 1];
        }
      } else if (shift[2] == 1) {
        ++diff[start];

        if (end + 1 < s.length()) {
          --diff[end + 1];
        }
      }
    }

    var result = new StringBuilder(s);

    var shiftBy = 0;
    for (var i = 0; i < s.length(); ++i) {
      shiftBy = (shiftBy + diff[i]) % 26;
      if (shiftBy < 0) {
        shiftBy += 26;
      }

      var shifted = (char) ('a' + ((s.charAt(i) - 'a' + shiftBy) % 26));
      result.setCharAt(i, shifted);
    }

    return result.toString();
  }
}
