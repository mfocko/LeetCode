#!/bin/bash

TYPE=$1
NAME=$2
SUFFIX=$3

echo "Type: $TYPE"
echo "Name: $NAME"
echo "Suffix: $SUFFIX"

$EDITOR $TYPE/$NAME.$SUFFIX
