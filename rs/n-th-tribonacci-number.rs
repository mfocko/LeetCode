impl Solution {
    pub fn tribonacci(mut n: i32) -> i32 {
        let mut seq = vec![0, 1, 1];

        while n >= 3 {
            let next = seq.iter().sum::<i32>();

            seq[0] = seq[1];
            seq[1] = seq[2];
            seq[2] = next;

            n -= 1;
        }

        seq[n as usize]
    }
}
