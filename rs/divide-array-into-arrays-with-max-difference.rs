impl Solution {
    pub fn divide_array(mut nums: Vec<i32>, k: i32) -> Vec<Vec<i32>> {
        nums.sort();

        let divisions: Vec<Vec<i32>> = nums.as_slice().chunks_exact(3).map(Vec::from).collect();

        for d in &divisions {
            let first = d.first().expect("must have first element");
            let last = d.last().expect("we are guaranteed triples");

            if last - first > k {
                return vec![];
            }
        }

        divisions
    }
}
