use std::collections::BTreeMap;

impl Solution {
    fn find_answer(counters: &BTreeMap<i32, i32>, key: i32) -> Vec<i32> {
        counters
            .iter()
            .filter_map(|(&player, &losses)| if losses == key { Some(player) } else { None })
            .collect()
    }

    pub fn find_winners(matches: Vec<Vec<i32>>) -> Vec<Vec<i32>> {
        let mut counters: BTreeMap<i32, i32> = BTreeMap::new();

        for m in &matches {
            let [win, lose, ..] = m[..] else {
                unreachable!()
            };

            counters.entry(win).or_insert(0);
            *counters.entry(lose).or_insert(0) += 1;
        }

        vec![0, 1]
            .iter()
            .map(|&c| Solution::find_answer(&counters, c))
            .collect()
    }
}
