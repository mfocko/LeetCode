use std::convert::TryInto;

impl Solution {
    pub fn pivot_index(nums: Vec<i32>) -> i32 {
        let mut from_left: i32 = 0;
        let mut from_right: i32 = nums.iter().sum();

        for (i, e) in nums.iter().enumerate() {
            from_right -= e;

            if from_left == from_right {
                return i.try_into().unwrap();
            }

            from_left += e;
        }

        -1
    }
}
