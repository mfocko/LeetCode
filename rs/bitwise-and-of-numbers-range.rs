impl Solution {
    fn discarded(mut left: i32, mut right: i32) -> i32 {
        let mut counter = 0;

        while left != right {
            left >>= 1;
            right >>= 1;

            counter += 1;
        }

        counter
    }

    pub fn range_bitwise_and(left: i32, right: i32) -> i32 {
        let discarded_bits = Solution::discarded(left, right);
        let mask = !((1 << discarded_bits) - 1);

        left & mask
    }
}
