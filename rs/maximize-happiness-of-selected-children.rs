use std::cmp;
use std::collections::BinaryHeap;

impl Solution {
    pub fn maximum_happiness_sum(happiness: Vec<i32>, k: i32) -> i64 {
        let mut q: BinaryHeap<i32> = BinaryHeap::from(happiness);

        let mut happiness: i64 = 0;

        let k: i64 = k.into();
        for i in 0..k {
            let next: i64 = q.pop().expect("k is smaller than length").into();
            happiness += cmp::max(0, next - i);
        }

        happiness
    }
}
