impl Solution {
    pub fn max_run_time(n: i32, mut batteries: Vec<i32>) -> i64 {
        batteries.sort();

        let mut total = batteries.iter().map(|x| *x as u64).sum::<u64>();
        let mut k: u64 = 0;

        while *batteries.last().unwrap() as u64 > total / (n as u64 - k) {
            total -= *batteries.last().unwrap() as u64;
            batteries.pop();

            k += 1;
        }

        (total / (n as u64 - k)) as i64
    }
}
