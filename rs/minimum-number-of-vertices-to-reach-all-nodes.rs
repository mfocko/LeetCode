impl Solution {
    pub fn find_smallest_set_of_vertices(n: i32, edges: Vec<Vec<i32>>) -> Vec<i32> {
        // It should be possible to use topological ordering + BFS, but…

        let mut is_reachable: Vec<bool> = vec![false; n as usize];
        for dst in edges.iter().map(|edge| edge[1] as usize) {
            is_reachable[dst] = true;
        }

        let mut result: Vec<i32> = vec![];
        for (i, &reachable) in is_reachable.iter().enumerate() {
            if !reachable {
                result.push(i as i32);
            }
        }

        result
    }
}
