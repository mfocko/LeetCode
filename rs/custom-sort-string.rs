use std::collections::HashMap;

impl Solution {
    pub fn custom_sort_string(order: String, s: String) -> String {
        let weights: HashMap<char, usize> =
            order.chars().enumerate().map(|(i, c)| (c, i)).collect();

        let mut s: Vec<char> = s.chars().collect();
        s.sort_unstable_by_key(|c| *weights.get(c).unwrap_or(&usize::MAX));

        s.into_iter().collect()
    }
}
