impl Solution {
    fn get_vowels(s: &str) -> usize {
        s.to_lowercase()
            .chars()
            .filter_map(|c| match c {
                'a' | 'e' | 'i' | 'o' | 'u' => Some(c),
                _ => None,
            })
            .count()
    }

    pub fn halves_are_alike(s: String) -> bool {
        let (a, b) = s.split_at(s.len() / 2);

        Solution::get_vowels(a) == Solution::get_vowels(b)
    }
}
