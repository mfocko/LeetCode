use std::collections::HashSet;

impl Solution {
    pub fn is_path_crossing(path: String) -> bool {
        let mut visited = HashSet::from([(0, 0)]);

        let mut x = 0;
        let mut y = 0;
        for d in path.chars() {
            match d {
                'N' => y -= 1,
                'S' => y += 1,
                'E' => x += 1,
                'W' => x -= 1,
                _ => unreachable!("invalid direction"),
            }

            if visited.contains(&(x, y)) {
                return true;
            }
            visited.insert((x, y));
        }

        false
    }
}
