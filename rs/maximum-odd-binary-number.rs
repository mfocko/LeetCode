use std::cmp;

impl Solution {
    pub fn maximum_odd_binary_number(s: String) -> String {
        let mut bits: Vec<char> = s.chars().collect();

        // sort in reverse order, all ones are at the beginning
        bits.sort_unstable_by_key(|&x| cmp::Reverse(x));

        // find the last one
        let i = bits.iter().rposition(|&x| x == '1').unwrap();

        // and swap with the last zero to obtain odd integer
        let last_index = bits.len() - 1;
        if i < last_index {
            bits.swap(i, last_index);
        }

        bits.into_iter().collect()
    }
}
