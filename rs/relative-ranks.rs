use std::collections::BinaryHeap;

impl Solution {
    fn pos_to_string(pos: usize) -> String {
        match pos {
            0 => String::from("Gold Medal"),
            1 => String::from("Silver Medal"),
            2 => String::from("Bronze Medal"),
            _ => format!("{}", pos + 1),
        }
    }

    pub fn find_relative_ranks(score: Vec<i32>) -> Vec<String> {
        let mut heap = BinaryHeap::new();

        for (i, s) in score.into_iter().enumerate() {
            heap.push((s, i));
        }

        let mut ranks = vec![String::new(); heap.len()];

        let mut i = 0;
        while let Some((_, idx)) = heap.pop() {
            ranks[idx] = Self::pos_to_string(i);
            i += 1;
        }

        ranks
    }
}
