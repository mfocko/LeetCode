use std::cmp;

impl Solution {
    pub fn find_min_arrow_shots(mut points: Vec<Vec<i32>>) -> i32 {
        points.sort_unstable_by_key(|p| p[0]);

        let mut used_arrows = 1;
        let mut max_x = points[0][1];

        for point in points.iter().skip(1) {
            if point[0] <= max_x {
                max_x = cmp::min(max_x, point[1]);
            } else {
                used_arrows += 1;
                max_x = point[1];
            }
        }

        used_arrows
    }
}
