use std::cmp;
use std::collections::HashSet;

impl Solution {
    pub fn find_max_k(nums: Vec<i32>) -> i32 {
        let mut seen = HashSet::<i32>::new();

        let mut max_k = -1;
        for x in &nums {
            if seen.contains(&-x) {
                max_k = cmp::max(max_k, x.abs());
            }

            seen.insert(*x);
        }

        max_k
    }
}
