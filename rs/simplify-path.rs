struct Solution {}
impl Solution {
    pub fn simplify_path(path: String) -> String {
        let mut simplified_path = Vec::<&str>::new();

        path.split('/').for_each(|seg| {
            if seg.is_empty() || seg == "." {
                return;
            }

            if seg == ".." {
                simplified_path.pop();
                return;
            }

            simplified_path.push(seg);
        });

        "/".to_owned() + &simplified_path.join("/")
    }
}

fn main() {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn one_directory() {
        assert_eq!(
            Solution::simplify_path("/home/".to_owned()),
            "/home".to_owned()
        );
    }

    #[test]
    fn just_root() {
        assert_eq!(Solution::simplify_path("/../".to_owned()), "/".to_owned());
    }

    #[test]
    fn consecutive_slashes() {
        assert_eq!(
            Solution::simplify_path("/home//foo/".to_owned()),
            "/home/foo".to_owned()
        );
    }
}
