impl Solution {
    pub fn sorted_squares(mut nums: Vec<i32>) -> Vec<i32> {
        for x in &mut nums {
            *x *= *x;
        }
        nums.sort_unstable();

        nums
    }
}
