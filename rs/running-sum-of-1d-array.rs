impl Solution {
    pub fn running_sum(nums: Vec<i32>) -> Vec<i32> {
        nums.iter()
            .scan(0, |s, &x| {
                *s = *s + x;
                Some(*s)
            })
            .collect()
    }
}
