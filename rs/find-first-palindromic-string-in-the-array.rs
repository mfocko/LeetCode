impl Solution {
    fn is_palindrome(word: &str) -> bool {
        word.chars()
            .take(word.len() >> 1)
            .eq(word.chars().rev().take(word.len() >> 1))
    }

    pub fn first_palindrome(words: Vec<String>) -> String {
        words
            .into_iter()
            .find(|s| Self::is_palindrome(s))
            .unwrap_or(String::from(""))
    }
}
