impl Solution {
    pub fn num_subarrays_with_sum(nums: Vec<i32>, goal: i32) -> i32 {
        let mut subarrays = 0;

        let mut sum = 0;
        let mut zeros = 0;

        let mut l = 0;
        for r in 0..nums.len() {
            sum += nums[r];

            while l < r && (nums[l] == 0 || sum > goal) {
                match nums[l] {
                    0 => {
                        zeros += 1;
                    }
                    1 => {
                        zeros = 0;
                    }
                    _ => unreachable!("either 0 or 1s"),
                }

                sum -= nums[l];
                l += 1;
            }

            if sum == goal {
                subarrays += 1 + zeros;
            }
        }

        subarrays
    }
}
