impl Solution {
    pub fn climb_stairs(n: i32) -> i32 {
        if n < 3 {
            return n;
        }

        let mut ways = vec![0, 1, 2];
        for k in 3..=n {
            ways.remove(0);
            ways.push(ways[0] + ways[1]);
        }

        ways[2]
    }
}
