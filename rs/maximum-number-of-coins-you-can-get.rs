impl Solution {
    pub fn max_coins(piles: Vec<i32>) -> i32 {
        let mut sorted_piles = piles.clone();
        sorted_piles.sort();

        sorted_piles
            .iter()
            .rev()
            .skip(1)
            .step_by(2)
            .take(piles.len() / 3)
            .sum()
    }
}
