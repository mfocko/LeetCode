impl Solution {
    pub fn largest_perimeter(mut nums: Vec<i32>) -> i64 {
        nums.sort();

        let mut sum: i64 = 0;
        let mut max_perimeter: i64 = -1;

        for x in &nums {
            let x = i64::from(*x);

            if x < sum {
                max_perimeter = x + sum;
            }

            sum += x;
        }

        max_perimeter
    }
}
