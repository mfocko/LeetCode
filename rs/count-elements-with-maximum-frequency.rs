use std::collections::HashMap;

impl Solution {
    pub fn max_frequency_elements(nums: Vec<i32>) -> i32 {
        let mut freqs: HashMap<i32, usize> = HashMap::new();
        for &x in &nums {
            *freqs.entry(x).or_insert(0) += 1;
        }

        let m = *freqs.values().max().unwrap();
        (m * freqs.iter().filter(|&(_, f)| *f == m).count()) as i32
    }
}
