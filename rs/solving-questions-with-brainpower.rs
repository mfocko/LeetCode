use std::cmp;

#[derive(Debug)]
struct Question {
    answered: i64,
    skipped: i64,
}

impl Question {
    fn get_with_offset(stack: &Vec<Self>, offset: i64) -> i64 {
        let i = stack.len() as i64 - offset - 1;
        if i < 0 || i >= stack.len() as i64 {
            return 0;
        }

        stack[i as usize].get()
    }

    pub fn new(q: &[i32], stack: &Vec<Self>) -> Self {
        Self {
            answered: q[0] as i64 + Self::get_with_offset(stack, q[1].into()),
            skipped: Self::get_with_offset(stack, 0),
        }
    }

    pub fn get(&self) -> i64 {
        cmp::max(self.answered, self.skipped)
    }
}

struct Solution {}
impl Solution {
    pub fn most_points(questions: Vec<Vec<i32>>) -> i64 {
        let mut stack: Vec<Question> = vec![];

        for input_q in questions.iter().rev() {
            stack.push(Question::new(&input_q, &stack));
        }

        stack.last().unwrap().get()
    }
}

fn main() {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_1() {
        assert_eq!(
            Solution::most_points(vec![vec![3, 2], vec![4, 3], vec![4, 4], vec![2, 5]]),
            5
        );
    }

    #[test]
    fn test_example_2() {
        assert_eq!(
            Solution::most_points(vec![
                vec![1, 1],
                vec![2, 2],
                vec![3, 3],
                vec![4, 4],
                vec![5, 5]
            ]),
            7
        );
    }
}
