use std::collections::{HashMap, HashSet};

impl Solution {
    pub fn is_isomorphic(s: String, t: String) -> bool {
        let mut projection: HashMap<char, char> = HashMap::new();
        let mut used: HashSet<char> = HashSet::new();

        for (l, r) in s.chars().zip(t.chars()) {
            match projection.get(&l) {
                Some(&expected_r) if expected_r != r => {
                    return false;
                }
                None => {
                    if used.contains(&r) {
                        return false;
                    }

                    projection.insert(l, r);
                    used.insert(r);
                }
                _ => { /* no-op */ }
            }
        }

        true
    }
}
