impl Solution {
    pub fn sequential_digits(low: i32, high: i32) -> Vec<i32> {
        let mut nums = vec![];

        for d in 1..=9 {
            let mut num = d;

            let mut next_d = d + 1;
            while num <= high && next_d <= 9 {
                num = 10 * num + next_d;

                if low <= num && num <= high {
                    nums.push(num);
                }

                next_d += 1;
            }
        }

        nums.sort();
        nums
    }
}
