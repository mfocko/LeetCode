use std::cmp;
use std::collections::HashMap;

impl Solution {
    pub fn min_steps(s: String, t: String) -> i32 {
        let mut counters: HashMap<char, i32> = HashMap::new();

        // needed
        for c in s.chars() {
            *counters.entry(c).or_insert(0) += 1;
        }

        // found
        for c in t.chars() {
            *counters.entry(c).or_insert(0) -= 1;
        }

        counters.values().map(|count| cmp::max(0, *count)).sum()
    }
}
