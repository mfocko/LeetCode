impl Solution {
    pub fn count_students(students: Vec<i32>, sandwiches: Vec<i32>) -> i32 {
        let mut students_by_sandwiches = vec![0; 2];
        for student in &students {
            students_by_sandwiches[*student as usize] += 1;
        }

        for sandwich in &sandwiches {
            let idx = *sandwich as usize;

            if students_by_sandwiches[idx] <= 0 {
                return students_by_sandwiches[(idx + 1) % 2];
            }

            students_by_sandwiches[idx] -= 1;
        }

        0
    }
}
