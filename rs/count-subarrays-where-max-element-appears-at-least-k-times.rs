impl Solution {
    pub fn count_subarrays(nums: Vec<i32>, mut k: i32) -> i64 {
        let m = *nums.iter().max().expect("1 ≤ nums.len() ≤ 10⁵");
        let mut counter: i64 = 0;

        let mut i: usize = 0;
        for j in 0..nums.len() {
            if nums[j] == m {
                k -= 1;
            }

            while k == 0 {
                if nums[i] == m {
                    k += 1;
                }

                i += 1;
            }

            counter += i as i64;
        }

        counter
    }
}
