use std::cmp;
use std::collections::HashMap;

impl Solution {
    pub fn least_interval(tasks: Vec<char>, n: i32) -> i32 {
        let mut freqs: HashMap<char, i32> = HashMap::new();
        let mut found_max = 0;

        for c in &tasks {
            let mut counter = freqs.entry(*c).or_insert(0);

            *counter += 1;
            found_max = cmp::max(found_max, *counter);
        }

        let mut time = (1 + n) * (found_max - 1);
        time += freqs.values().filter(|&&c| c == found_max).count() as i32;

        cmp::max(tasks.len().try_into().unwrap(), time)
    }
}
