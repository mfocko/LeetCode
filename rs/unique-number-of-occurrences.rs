use std::collections::{HashMap, HashSet};

impl Solution {
    pub fn unique_occurrences(arr: Vec<i32>) -> bool {
        // count frequencies
        let mut freqs: HashMap<i32, i32> = HashMap::new();
        for x in &arr {
            *freqs.entry(*x).or_insert(0) += 1;
        }

        freqs.values().collect::<HashSet<_>>().len() == freqs.len()
    }
}
