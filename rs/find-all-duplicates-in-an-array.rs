impl Solution {
    pub fn find_duplicates(mut nums: Vec<i32>) -> Vec<i32> {
        let mut duplicates = vec![];

        for i in 0..nums.len() {
            let index = nums[i].abs() - 1;

            if nums[index as usize] < 0 {
                duplicates.push(index + 1);
            } else {
                nums[index as usize] *= -1;
            }
        }

        duplicates
    }
}
