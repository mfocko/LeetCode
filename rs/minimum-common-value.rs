impl Solution {
    pub fn get_common(xs: Vec<i32>, ys: Vec<i32>) -> i32 {
        if xs.len() > ys.len() {
            return Self::get_common(ys, xs);
        }

        xs.into_iter()
            .find(|x| ys.binary_search(&x).is_ok())
            .unwrap_or(-1)
    }
}
