use std::collections::HashSet;

impl Solution {
    pub fn intersection(mut nums1: Vec<i32>, mut nums2: Vec<i32>) -> Vec<i32> {
        // if ‹nums1› is bigger, swap them
        if nums1.len() > nums2.len() {
            return Self::intersection(nums2, nums1);
        }

        let nums: HashSet<i32> = nums1.into_iter().collect();
        nums2.sort_unstable();

        nums.into_iter()
            .filter(|x| nums2.binary_search(x).is_ok())
            .collect()
    }
}
