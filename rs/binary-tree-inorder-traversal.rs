// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
//
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
// }
use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    fn inorder(values: &mut Vec<i32>, node: Option<&Rc<RefCell<TreeNode>>>) {
        match node {
            None => {}
            Some(n) => {
                Solution::inorder(values, (*n).borrow().left.as_ref());
                values.push((*n).borrow().val);
                Solution::inorder(values, (*n).borrow().right.as_ref());
            }
        }
    }

    pub fn inorder_traversal(root: Option<Rc<RefCell<TreeNode>>>) -> Vec<i32> {
        let mut values: Vec<i32> = Vec::new();

        Solution::inorder(&mut values, root.as_ref());

        values
    }
}
