use std::cmp::Reverse;
use std::collections::BTreeMap;

impl Solution {
    pub fn frequency_sort(s: String) -> String {
        let mut freqs: BTreeMap<_, usize> = BTreeMap::new();

        s.as_str().chars().for_each(|c| {
            let current = freqs.get(&c).unwrap_or(&0);
            freqs.insert(c, 1 + current);
        });

        let mut frequencies: Vec<_> = freqs.iter().collect();
        frequencies.sort_by_key(|&(_, count)| Reverse(count));

        frequencies.iter().fold(String::new(), |s, (c, count)| {
            s + &c.to_string().repeat(**count)
        })
    }
}
