// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
//
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
// }

use std::cell::RefCell;
use std::cmp;
use std::rc::Rc;

fn _max_depth(root: Option<Rc<RefCell<TreeNode>>>, depth: i32) -> i32 {
    match root {
        None => depth,
        Some(node) => cmp::max(
            _max_depth(node.borrow().left.clone(), depth + 1),
            _max_depth(node.borrow().right.clone(), depth + 1),
        ),
    }
}

impl Solution {
    pub fn max_depth(root: Option<Rc<RefCell<TreeNode>>>) -> i32 {
        _max_depth(root, 0)
    }
}
