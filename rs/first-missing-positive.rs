impl Solution {
    pub fn first_missing_positive(nums: Vec<i32>) -> i32 {
        let n = nums.len() + 1;
        let mut seen = vec![false; n];

        for x in nums.into_iter() {
            if x <= 0 || (n as i32) <= x {
                continue;
            }

            seen[x as usize - 1] = true;
        }

        1 + seen
            .into_iter()
            .enumerate()
            .find(|(_, val)| !val)
            .expect("there's always a missing positive integer")
            .0 as i32
    }
}
