use std::cmp;

impl Solution {
    pub fn time_required_to_buy(tickets: Vec<i32>, k: i32) -> i32 {
        let threshold = tickets[k as usize];

        let from_left: i32 = (0..=k as usize)
            .map(|i| cmp::min(tickets[i], threshold))
            .sum();
        let from_right: i32 = (k as usize + 1..tickets.len())
            .map(|i| cmp::min(tickets[i], threshold - 1))
            .sum();

        from_left + from_right
    }
}
