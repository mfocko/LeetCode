# @param {Integer[]} cost
# @return {Integer}
def min_cost_climbing_stairs(cost)
    def get(cost, k)
        if k < 0 || k >= cost.size then
            return 0
        end
        return cost[k]
    end

    i = cost.size - 1
    while i >= 0 do
        cost[i] += [get(cost, i + 1), get(cost, i + 2)].min
        i -= 1
    end

    return cost.take(2).min
end

RSpec.describe "min_cost_climbing_stairs" do
    it "of [10,15,20] is 15" do
        expect(min_cost_climbing_stairs([10,15,20])).to eq(15)
    end

    it "of [1,100,1,1,1,100,1,1,100,1] is 6" do
        expect(min_cost_climbing_stairs([1,100,1,1,1,100,1,1,100,1])).to eq(6)
    end
end
