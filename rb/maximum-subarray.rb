# @param {Integer[]} nums
# @return {Integer}
def max_sub_array(nums)
    if nums.empty? then
        return nil
    end

    sum, running_sum = nums.first, nums.first
    nums.drop(1).each { |x|
        running_sum = [running_sum + x, x].max
        sum = [sum, running_sum].max
    }

    return sum
end

RSpec.describe "max_sub_array of " do
    it "[-2,1,-3,4,-1,2,1,-5,4] is 6" do
        expect(max_sub_array([-2,1,-3,4,-1,2,1,-5,4])).to eq(6)
    end

    it "[1] is 1" do
        expect(max_sub_array([1])).to eq(1)
    end

    it "[5,4,-1,7,8] is 23" do
        expect(max_sub_array([5,4,-1,7,8])).to eq(23)
    end
end
