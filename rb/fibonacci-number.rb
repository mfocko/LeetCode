# @param {Integer} n
# @return {Integer}
def fib(n)
    if n == 0 then
        return 0
    end

    prev, current = 0, 1
    (2..n).each {
        prev, current = current, prev + current
    }
    return current
end

RSpec.describe "fib of " do
    it "0 is 0" do
        expect(fib(0)).to eq(0)
    end

    it "1 is 1" do
        expect(fib(1)).to eq(1)
    end

    it "2 is 1" do
        expect(fib(2)).to eq(1)
    end

    it "3 is 2" do
        expect(fib(3)).to eq(2)
    end

    it "4 is 3" do
        expect(fib(4)).to eq(3)
    end

    it "5 is 5" do
        expect(fib(5)).to eq(5)
    end
end
