# Definition for a binary tree node.
# class TreeNode
#     attr_accessor :val, :left, :right
#     def initialize(val = 0, left = nil, right = nil)
#         @val = val
#         @left = left
#         @right = right
#     end
# end
# @param {TreeNode} root
# @param {Integer} val
# @return {TreeNode}
def insert_into_bst(root, val)
    new_node = TreeNode.new(val)
    if root == nil then
        return new_node
    end

    node = root
    while (val < node.val && node.left != nil) || (val >= node.val && node.right != nil) do
        if val < node.val then
            node = node.left
        else
            node = node.right
        end
    end

    # insert the node
    if val < node.val then
        node.left = new_node
    else
        node.right = new_node
    end

    return root
end
