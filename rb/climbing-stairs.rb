# @param {Integer} n
# @return {Integer}
def climb_stairs(n)
    possible_ways = [0, 1, 2]

    k = 3
    while possible_ways.size <= n do
        possible_ways.push(possible_ways[k - 1] + possible_ways[k - 2])
        k += 1
    end

    return possible_ways[n]
end

RSpec.describe "climb_stairs" do
    it "1 is 1" do
        expect(climb_stairs(1)).to eq(1)
    end

    it "2 is 2" do
        expect(climb_stairs(2)).to eq(2)
    end

    it "3 is 3" do
        expect(climb_stairs(3)).to eq(3)
    end
end
