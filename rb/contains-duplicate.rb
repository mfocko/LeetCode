# @param {Integer[]} nums
# @return {Boolean}
def contains_duplicate(nums)
    encountered = Set.new

    nums.each { |x|
        if encountered.add?(x) == nil then
            return true
        end
    }

    return false
end

RSpec.describe "contains_duplicate" do
    it "nums = [1,2,3,1] contains" do
        expect(contains_duplicate([1,2,3,1])).to be true
    end

    it "nums = [1,2,3,4] doesn't contain" do
        expect(contains_duplicate([1,2,3,4])).to be false
    end

    it "nums = [1,1,1,3,3,4,3,2,4,2] contains" do
        expect(contains_duplicate([1,1,1,3,3,4,3,2,4,2])).to be true
    end
end
