# @param {Integer} n
# @return {Integer}
def tribonacci(n)
    sequence = [0, 1, 1]
    if n < 3 then
        return sequence[n]
    end

    (3..n).each {
        sequence = sequence.drop(1) + [sequence.sum]
    }
    return sequence.last
end

RSpec.describe "tribonacci of " do
    it "0 is 0" do
        expect(tribonacci(0)).to eq(0)
    end

    it "1 is 1" do
        expect(tribonacci(1)).to eq(1)
    end

    it "2 is 1" do
        expect(tribonacci(2)).to eq(1)
    end

    it "3 is 2" do
        expect(tribonacci(3)).to eq(2)
    end

    it "4 is 4" do
        expect(tribonacci(4)).to eq(4)
    end

    it "5 is 7" do
        expect(tribonacci(5)).to eq(7)
    end

    it "25 is 1389537" do
        expect(tribonacci(25)).to eq(1389537)
    end
end
