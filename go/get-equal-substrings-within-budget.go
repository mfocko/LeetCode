package main

func equalSubstring(s string, t string, maxCost int) int {
	abs := func(x int) int {
		return max(x, -x)
	}

	getCost := func(i int) int {
		return abs(int(s[i]) - int(t[i]))
	}

	mostOptimal := 0

	begin := 0

	cost := 0
	for i, _ := range t {
		cost += getCost(i)

		for cost > maxCost {
			cost -= getCost(begin)
			begin++
		}

		mostOptimal = max(mostOptimal, i-begin+1)
	}

	return mostOptimal
}
