package main

import "slices"

func minDifference(nums []int) int {
	if len(nums) <= 4 {
		return 0
	}

	slices.Sort(nums)

	diff := nums[len(nums)-1] - nums[0]
	for i := 0; i < 4; i++ {
		diff = min(diff, nums[len(nums)-4+i]-nums[i])
	}

	return diff
}
