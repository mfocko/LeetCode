package main

func maximumGain(s string, x int, y int) int {
	reverseString := func(s string) string {
		runes := []rune(s)
		for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
			runes[i], runes[j] = runes[j], runes[i]
		}
		return string(runes)
	}

	// swap for bigger score
	if x < y {
		x, y = y, x
		s = reverseString(s)
	}

	aCount, bCount, points := 0, 0, 0
	for _, c := range s {
		if c == 'a' {
			aCount++
		} else if c == 'b' {
			if aCount > 0 {
				aCount--
				points += x
			} else {
				bCount++
			}
		} else {
			points += y * min(bCount, aCount)
			aCount = 0
			bCount = 0
		}
	}

	points += y * min(bCount, aCount)

	return points
}
