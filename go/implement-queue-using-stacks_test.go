package main

import (
	"testing"
)

func Test_ImplementQueueUsingStacks_Empty(t *testing.T) {
	obj := NewQueue()

	if !obj.Empty() {
		t.Error("Queue should be empty")
	}
}

func Test_ImplementQueueUsingStacks_Basic(t *testing.T) {
	q := NewQueue()

	q.Push(1)
	q.Push(2)

	if q.Peek() != 1 {
		t.Error("1 should be at the front of the queue")
	}
	if q.Pop() != 1 {
		t.Error("1 should've been removed from the queue")
	}

	if q.Empty() {
		t.Error("Queue shouldn't be empty")
	}

	q.Push(3)

	if q.Pop() != 2 {
		t.Error("Queue should've removed 2")
	}

	if q.Empty() {
		t.Error("Queue shouldn't be empty")
	}

	if q.Pop() != 3 {
		t.Error("Queue should've removed 3")
	}

	if !q.Empty() {
		t.Error("Queue should be empty")
	}
}
