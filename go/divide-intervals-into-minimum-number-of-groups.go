package main

func minGroups(intervals [][]int) int {
	minStart, maxEnd := intervals[0][0], intervals[0][1]
	for _, interval := range intervals {
		start, end := interval[0], interval[1]

		minStart = min(minStart, start)
		maxEnd = max(maxEnd, end)
	}

	hits := make([]int, maxEnd+2)
	for _, interval := range intervals {
		start, end := interval[0], interval[1]
		hits[start]++
		hits[end+1]--
	}

	concurrent, maxConcurrent := 0, 0
	for i := minStart; i <= maxEnd; i++ {
		concurrent += hits[i]

		maxConcurrent = max(maxConcurrent, concurrent)
	}

	return maxConcurrent
}
