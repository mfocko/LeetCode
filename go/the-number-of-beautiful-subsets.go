package main

import (
	"slices"
)

func beautifulSubsets(nums []int, k int) int {
	seen := make(map[int]int)

	var dfs func(int) int
	dfs = func(i int) int {
		// BASE: Got to the end of the slice
		if i >= len(nums) {
			return 1
		}

		// Initialize with skipping the current number
		foundSubsets := dfs(i + 1)

		// Check if we can include the current number
		if seen[nums[i]-k] == 0 && seen[nums[i]+k] == 0 {
			seen[nums[i]] += 1
			foundSubsets += dfs(i + 1)
			seen[nums[i]] -= 1
		}

		return foundSubsets
	}

	slices.Sort(nums)
	return dfs(0) - 1
}
