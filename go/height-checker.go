package main

func heightChecker(heights []int) int {
	// count the heights
	var counts [100]int
	for _, h := range heights {
		counts[h-1]++
	}

	incorrect := 0

	expectedH := 1
	for _, h := range heights {
		for expectedH <= 100 && counts[expectedH-1] == 0 {
			expectedH++
		}
		counts[expectedH-1]--

		if expectedH != h {
			incorrect++
		}
	}

	return incorrect
}
