package main

func canArrange(arr []int, k int) bool {
	counters := make([]int, k)
	for _, x := range arr {
		counters[(x%k+k)%k]++
	}

	for i := 0; i < k && counters[i] != -1; i++ {
		j := (k - i) % k
		if counters[i] != counters[j] || (i == j && counters[i]%2 == 1) {
			return false
		}

		counters[i], counters[j] = -1, -1
	}

	return true
}
