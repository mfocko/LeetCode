package main

func removeDuplicates(nums []int) int {
	l := 1

	for r := 1; r < len(nums); r++ {
		if nums[r-1] != nums[r] {
			nums[l] = nums[r]
			l++
		}
	}

	return l
}
