package main

import (
	"slices"
)

func specialArray(nums []int) int {
	bsearch := func(key int) int {
		l := 0
		r := len(nums) - 1

		idx := len(nums)
		for l <= r {
			mid := (l + r) / 2

			if nums[mid] >= key {
				idx = mid
				r = mid - 1
			} else {
				l = mid + 1
			}
		}

		return idx
	}

	slices.Sort(nums)
	for i, _ := range nums {
		k := bsearch(i + 1)

		if len(nums)-k == i+1 {
			return i + 1
		}
	}

	return -1
}
