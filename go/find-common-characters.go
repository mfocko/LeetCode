package main

func commonChars(words []string) []string {
	A := int('a')
	countCharacters := func(counters []int, word string) {
		for _, c := range word {
			counters[int(c)-A]++
		}
	}
	setCommon := func(common []int, word []int) {
		for i, count := range word {
			common[i] = min(common[i], count)
		}
	}

	commonCounters := make([]int, 26)
	countCharacters(commonCounters, words[0])

	for _, word := range words {
		wordCounters := make([]int, 26)

		countCharacters(wordCounters, word)
		setCommon(commonCounters, wordCounters)
	}

	var result []string
	for i, count := range commonCounters {
		for count > 0 {
			result = append(result, string(rune(A+i)))
			count--
		}
	}
	return result
}
