package main

func pairSum(head *ListNode) int {
	findMidAndBuild := func() ([]int, *ListNode) {
		stack := make([]int, 0)

		slow, fast := head, head
		for fast != nil {
			stack = append(stack, slow.Val)
			slow, fast = slow.Next, fast.Next.Next
		}

		return stack, slow
	}

	stack, node := findMidAndBuild()

	foundMax, idx := 0, len(stack)-1
	for node != nil {
		foundMax = max(foundMax, stack[idx]+node.Val)

		node = node.Next
		idx--
	}

	return foundMax
}
