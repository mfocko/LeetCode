package main

func findTheLongestSubstring(s string) int {
	getMask := func(c rune) int {
		switch c {
		case 'a':
			return 1
		case 'e':
			return 2
		case 'i':
			return 4
		case 'o':
			return 8
		case 'u':
			return 16
		default:
			return 0
		}
	}

	longest := 0

	indices := make([]int, 32)
	for i, _ := range indices {
		indices[i] = -1
	}

	xor := 0
	for i, c := range s {
		xor ^= getMask(c)

		if indices[xor] == -1 && xor != 0 {
			indices[xor] = i
		}

		longest = max(longest, i-indices[xor])
	}

	return longest
}
