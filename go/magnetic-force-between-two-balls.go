package main

import "slices"

func maxDistance(position []int, m int) int {
	placed := func(distance int) bool {
		counter := 1

		last := position[0]
		for _, p := range position {
			if p-last >= distance {
				counter++
				last = p
			}

			if counter >= m {
				return true
			}
		}

		return false
	}

	slices.Sort(position)

	low := 1
	high := (position[len(position)-1] - position[0]) / (m - 1)

	foundMax := 1
	for low <= high {
		mid := low + (high-low)/2
		if placed(mid) {
			foundMax = mid
			low = mid + 1
		} else {
			high = mid - 1
		}
	}

	return foundMax
}
