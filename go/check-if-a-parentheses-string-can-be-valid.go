package main

func canBeValid(s string, locked string) bool {
	n := len(s)
	if n%2 == 1 {
		// can't pair brackets
		return false
	}

	open, unlocked, balance := 0, 0, 0
	for i := range n {
		if locked[i] == '0' {
			unlocked++
		} else if s[i] == '(' {
			open++
		} else if s[i] == ')' {
			if open > 0 {
				open--
			} else if unlocked > 0 {
				unlocked--
			} else {
				return false
			}
		}
	}

	for i := n - 1; i >= 0 && (unlocked != 0 || open != 0); i-- {
		if locked[i] == '0' {
			balance--
			unlocked--
		} else if s[i] == '(' {
			balance++
			open--
		} else if s[i] == ')' {
			balance--
		}

		if balance > 0 {
			return false
		}
	}

	return open <= 0
}
