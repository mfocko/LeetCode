package main

import (
	"cmp"

	pq "github.com/emirpasic/gods/v2/queues/priorityqueue"
)

type Num struct {
	value int
	index int
}

func cmpNums(x, y Num) int {
	return cmp.Or(cmp.Compare(x.value, y.value), cmp.Compare(x.index, y.index))
}

func getFinalState(nums []int, k int, multiplier int) []int {
	numbers := pq.NewWith(cmpNums)
	for i, num := range nums {
		numbers.Enqueue(Num{value: num, index: i})
	}

	for range k {
		candidate, ok := numbers.Dequeue()
		if !ok {
			panic("there should always be at least one candidate")
		}

		nums[candidate.index] *= multiplier
		numbers.Enqueue(Num{value: candidate.value * multiplier, index: candidate.index})
	}

	return nums
}
