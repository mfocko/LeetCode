package main

const (
	NOT_SEEN int = -1
	SERVER   int = 1
)

func countServers(grid [][]int) int {
	count := 0

	inCol := make([]int, len(grid[0]))
	lastInRow := make([]int, len(grid))
	for y, _ := range lastInRow {
		lastInRow[y] = NOT_SEEN
	}

	for y, row := range grid {
		countInRow := 0
		for x, state := range row {
			if state == SERVER {
				countInRow++
				inCol[x]++
				lastInRow[y] = x
			}
		}

		if countInRow > 1 {
			count += countInRow
			lastInRow[y] = NOT_SEEN
		}
	}

	for y, _ := range grid {
		if lastInRow[y] != NOT_SEEN && inCol[lastInRow[y]] > 1 {
			count++
		}
	}

	return count
}
