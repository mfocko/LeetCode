package main

import (
	"cmp"

	pq "github.com/emirpasic/gods/v2/queues/priorityqueue"
)

func maxKelements(nums []int, k int) int64 {
	intCeil := func(whole, remainder int) int {
		if remainder != 0 {
			return whole + 1
		}
		return whole
	}

	descending := func(a, b int) int {
		return -cmp.Compare(a, b)
	}

	q := pq.NewWith(descending)
	for _, x := range nums {
		q.Enqueue(x)
	}

	score := int64(0)
	for i := 0; i < k; i++ {
		num, _ := q.Dequeue()
		score += int64(num)

		q.Enqueue(intCeil(num/3, num%3))
	}
	return score
}
