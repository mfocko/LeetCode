package main

func minOperations(logs []string) int {
	depth := 0

	for _, dir := range logs {
		if dir == "../" {
			depth = max(0, depth-1)
		} else if dir == "./" {
			/* no-op */
		} else {
			depth++
		}
	}

	return depth
}
