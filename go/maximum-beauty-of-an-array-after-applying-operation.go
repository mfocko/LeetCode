package main

import "slices"

func maximumBeauty(nums []int, k int) int {
	if len(nums) == 1 {
		return 1
	}

	maxValue := slices.Max(nums)

	count := make([]int, maxValue+1)
	for _, n := range nums {
		count[max(0, n-k)]++
		count[min(n+k+1, maxValue)]--
	}

	maxBeauty, runningSum := 0, 0
	for _, x := range count {
		runningSum += x
		maxBeauty = max(maxBeauty, runningSum)
	}

	return maxBeauty
}
