package main

import (
	pq "github.com/emirpasic/gods/v2/queues/priorityqueue"
)

type NumberContainers struct {
	mapping map[int]int
	indices map[int]*pq.Queue[int]
}

func Constructor() NumberContainers {
	return NumberContainers{
		mapping: make(map[int]int),
		indices: make(map[int]*pq.Queue[int]),
	}
}

func (this *NumberContainers) Change(index int, number int) {
	this.mapping[index] = number

	indices, ok := this.indices[number]
	if !ok {
		indices = pq.New[int]()
		this.indices[number] = indices
	}
	indices.Enqueue(index)
}

func (this *NumberContainers) Find(number int) int {
	indices, ok := this.indices[number]
	if !ok {
		return -1
	}

	for idx, ok := indices.Peek(); ok; idx, ok = indices.Peek() {
		if this.mapping[idx] == number {
			return idx
		}

		indices.Dequeue()
	}

	return -1
}
