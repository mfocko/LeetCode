package main

import (
	"slices"
)

func minMovesToSeat(seats []int, students []int) int {
	abs := func(value int) int {
		return max(-value, value)
	}

	// sort them to get minimal distances
	slices.Sort(seats)
	slices.Sort(students)

	moves := 0
	for i, seat := range seats {
		student := students[i]
		moves += abs(seat - student)
	}

	return moves
}
