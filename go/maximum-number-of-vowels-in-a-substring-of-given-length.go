package main

func maxVowels(s string, k int) int {
	isVowel := func(c byte) bool {
		return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'
	}
	countFirst := func() int {
		count := 0
		for i := 0; i < k; i++ {
			if isVowel(s[i]) {
				count++
			}
		}
		return count
	}

	count := countFirst()

	maxCount := count
	for i := k; i < len(s); i++ {
		if isVowel(s[i-k]) {
			count--
		}

		if isVowel(s[i]) {
			count++
		}

		maxCount = max(maxCount, count)
	}

	return maxCount
}
