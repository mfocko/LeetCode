package main

func isArraySpecial(nums []int, queries [][]int) []bool {
	getMaxReach := func() []int {
		reach := make([]int, len(nums))

		end := 0
		for start := 0; start < len(nums); start++ {
			end = max(end, start)

			for end < len(nums)-1 && nums[end]%2 != nums[end+1]%2 {
				end++
			}

			reach[start] = end
		}

		return reach
	}
	maxReach := getMaxReach()

	ans := make([]bool, len(queries))
	for i, q := range queries {
		start, end := q[0], q[1]

		ans[i] = end <= maxReach[start]
	}

	return ans
}
