package main

func bstToGst(root *TreeNode) *TreeNode {
	var rec func(*TreeNode, int) int
	rec = func(node *TreeNode, toAdd int) int {
		if node == nil {
			return toAdd
		}

		// recurse to right
		toAdd = rec(node.Right, toAdd)

		// add to the current value
		originalValue := node.Val
		node.Val += toAdd

		// pass new sum to the left
		toAdd = rec(node.Left, toAdd+originalValue)

		// return new sum
		return toAdd
	}
	rec(root, 0)

	return root
}
