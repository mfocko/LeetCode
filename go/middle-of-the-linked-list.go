package main

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func middleNode(head *ListNode) *ListNode {
	x := head
	y := head

	for y != nil && y.Next != nil {
		x = x.Next
		y = y.Next.Next
	}

	return x
}
