package main

func hIndex(citations []int) int {
	counters := make([]int, len(citations)+1)

	for _, citated := range citations {
		counters[min(len(citations), citated)]++
	}

	total := 0
	for i := len(citations); i >= 0; i-- {
		total += counters[i]
		if total >= i {
			return i
		}
	}

	panic("at least one counter should always match")
}
