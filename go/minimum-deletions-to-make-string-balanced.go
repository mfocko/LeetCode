package main

func minimumDeletions(s string) int {
	dp := make([]int, len(s)+1)

	bs := 0
	for i, c := range s {
		if c == 'b' {
			dp[i+1] = dp[i]
			bs++
		} else {
			dp[i+1] = min(dp[i]+1, bs)
		}
	}

	return dp[len(s)]
}
