package main

import (
	pq "github.com/emirpasic/gods/v2/queues/priorityqueue"
)

func minOperations(nums []int, k int) int {
	t := int64(k)

	q := pq.New[int64]()
	for _, x := range nums {
		q.Enqueue(int64(x))
	}

	operations := 0
	for m, ok := q.Peek(); ok && m < t; m, ok = q.Peek() {
		x, _ := q.Dequeue()
		y, _ := q.Dequeue()

		q.Enqueue(min(x, y)*2 + max(x, y))
		operations++
	}

	return operations
}
