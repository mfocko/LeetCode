package main

const MOD int = 1_000_000_007

func numTilings(n int) int {
	dp := [...]int{0, 1, 2, 5}

	for n >= 4 {
		dp[0], dp[1], dp[2], dp[3] = dp[1], dp[2], dp[3], (2*dp[3]+dp[1])%MOD
		n--
	}

	return dp[n]
}
