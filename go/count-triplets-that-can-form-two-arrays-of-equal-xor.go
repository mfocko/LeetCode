package main

func countTriplets(arr []int) int {
	precomputePrefix := func() []int {
		prefix := append([]int{0}, arr...)

		for i, _ := range arr {
			prefix[i+1] ^= prefix[i]
		}

		return prefix
	}

	prefix := precomputePrefix()

	triplets := 0
	for left, _ := range prefix {
		for right := left + 1; right < len(prefix); right++ {
			if prefix[left] == prefix[right] {
				triplets += right - left - 1
			}
		}
	}

	return triplets
}
