package main

func minHeightShelves(books [][]int, shelfWidth int) int {
	dp := make([]int, len(books)+1)

	dp[0], dp[1] = 0, books[0][1]

	for i := 2; i <= len(books); i++ {
		remainingWidth := shelfWidth - books[i-1][0]
		maxHeight := books[i-1][1]

		dp[i] = books[i-1][1] + dp[i-1]

		for j := i - 1; j > 0 && remainingWidth-books[j-1][0] >= 0; j-- {
			maxHeight = max(maxHeight, books[j-1][1])
			remainingWidth -= books[j-1][0]

			dp[i] = min(dp[i], maxHeight+dp[j-1])
		}
	}

	return dp[len(books)]
}
