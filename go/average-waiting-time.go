package main

func averageWaitingTime(customers [][]int) float64 {
	waitingTime := 0

	idleAt := 0
	for _, customer := range customers {
		arrives, length := customer[0], customer[1]

		idleAt = max(idleAt, arrives) + length
		waitingTime += idleAt - arrives
	}

	return float64(waitingTime) / float64(len(customers))
}
