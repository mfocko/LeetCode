package main

func oddEvenList(head *ListNode) *ListNode {
	dummies := []ListNode{ListNode{Val: 0, Next: nil}, ListNode{Val: 0, Next: nil}}
	tails := []*ListNode{&dummies[0], &dummies[1]}

	for idx := 0; head != nil; idx++ {
		tails[idx%2].Next = head
		tails[idx%2] = head

		head = head.Next
	}

	tails[0].Next = dummies[1].Next
	tails[1].Next = nil
	return dummies[0].Next
}
