package main

func findCenter(edges [][]int) int {
	u, v := edges[0][0], edges[0][1]
	s, t := edges[1][0], edges[1][1]

	if u == s || u == t {
		return u
	}
	return v
}
