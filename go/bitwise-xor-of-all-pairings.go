package main

func xorAllNums(nums1 []int, nums2 []int) int {
	xorFold := func(nums []int, otherLength int) int {
		acc := 0
		if otherLength%2 == 0 {
			return acc
		}

		for _, num := range nums {
			acc ^= num
		}

		return acc
	}

	xor1, xor2 := xorFold(nums1, len(nums2)), xorFold(nums2, len(nums1))
	return xor1 ^ xor2
}
