package main

import "slices"

func numberOfSubstrings(s string) int {
	total := 0

	lastPos := []int{-1, -1, -1}
	for i, c := range s {
		lastPos[c-'a'] = i
		total += 1 + slices.Min(lastPos)
	}

	return total
}
