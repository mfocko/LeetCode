package main

func checkSubstring(s string, i, j int) int {
	if i < 0 || j >= len(s) {
		return 0
	}

	count := 0

	for i >= 0 && j < len(s) && s[i] == s[j] {
		count += 1

		i -= 1
		j += 1
	}

	return count
}

func countSubstrings(s string) int {
	count := 0

	for i, _ := range s {
		count += checkSubstring(s, i, i)
		count += checkSubstring(s, i, i+1)
	}

	return count
}
