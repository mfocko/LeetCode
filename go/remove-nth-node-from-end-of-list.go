package main

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func removeNthFromEnd(lst *ListNode, n int) *ListNode {
	toRemove := lst
	head := lst

	for i := 0; i < n; i++ {
		head = head.Next
	}

	if head == nil {
		return toRemove.Next
	}

	for head.Next != nil {
		toRemove = toRemove.Next
		head = head.Next
	}

	toRemove.Next = toRemove.Next.Next
	return lst
}
