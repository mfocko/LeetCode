package main

type CustomStack struct {
	maxSize int
	stack   []int
	size    int
}

func Constructor(maxSize int) CustomStack {
	return CustomStack{
		maxSize: maxSize,
		stack:   make([]int, maxSize),
		size:    0,
	}
}

func (this *CustomStack) Push(x int) {
	if this.size >= this.maxSize {
		return
	}

	this.stack[this.size] = x
	this.size++
}

func (this *CustomStack) Pop() int {
	if this.size <= 0 {
		return -1
	}

	this.size--
	return this.stack[this.size]
}

func (this *CustomStack) Increment(k int, val int) {
	for i := 0; i < k && i < this.size; i++ {
		this.stack[i] += val
	}
}
