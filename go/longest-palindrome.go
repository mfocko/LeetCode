package main

func longestPalindrome(s string) int {
	getFreqs := func() map[rune]int {
		freqs := make(map[rune]int)
		for _, c := range s {
			freqs[c]++
		}
		return freqs
	}
	freqs := getFreqs()

	length := 0

	usedOdd := false
	for _, count := range freqs {
		length += 2 * (count / 2)

		if !usedOdd && count%2 == 1 {
			length += 1
			usedOdd = true
		}
	}

	return length
}
