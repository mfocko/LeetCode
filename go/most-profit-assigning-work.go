package main

import (
	"slices"
)

func maxProfitAssignment(difficulty []int, profit []int, worker []int) int {
	maxAbility := slices.Max(worker)
	jobs := make([]int, maxAbility+1)

	for i, d := range difficulty {
		if d > maxAbility {
			continue
		}
		jobs[d] = max(jobs[d], profit[i])
	}

	for i := 1; i <= maxAbility; i++ {
		jobs[i] = max(jobs[i], jobs[i-1])
	}

	total := 0
	for _, ability := range worker {
		total += jobs[ability]
	}

	return total
}
