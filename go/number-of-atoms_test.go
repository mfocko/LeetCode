package main

import "testing"

func _test_NumberOfAtoms(t *testing.T, input, expected string) {
	result := countOfAtoms(input)
	if result != expected {
		t.Errorf("Expected ‹%s› for ‹%s›, but got: ‹%s›!", expected, input, result)
	}
}

func Test_NumberOfAtoms_Easy(t *testing.T) {
	_test_NumberOfAtoms(t, "H2", "H2")
	_test_NumberOfAtoms(t, "O2", "O2")
	_test_NumberOfAtoms(t, "HO", "HO")
	_test_NumberOfAtoms(t, "H2O", "H2O")
	_test_NumberOfAtoms(t, "H2SO4", "H2O4S")
}

func Test_NumberOfAtoms_Medium(t *testing.T) {
	_test_NumberOfAtoms(t, "Mg(OH)2", "H2MgO2")
	_test_NumberOfAtoms(t, "C6H5OH", "C6H6O")
	_test_NumberOfAtoms(t, "C6H5CH3", "C7H8")
}

func Test_NumberOfAtoms_Hard(t *testing.T) {
	_test_NumberOfAtoms(t, "K4(ON(SO3)2)2", "K4N2O14S4")
}
