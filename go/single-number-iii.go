package main

func singleNumber(nums []int) []int {
	reduce := func() int {
		xor := 0
		for _, x := range nums {
			xor ^= x
		}
		return xor
	}
	xor := reduce()

	findMask := func() int {
		mask := 1
		for (xor & mask) == 0 {
			mask <<= 1
		}
		return mask
	}
	mask := findMask()

	a := 0
	b := 0
	for _, x := range nums {
		if (x & mask) != 0 {
			a ^= x
		} else {
			b ^= x
		}
	}

	return []int{a, b}
}
