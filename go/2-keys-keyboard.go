package main

func minSteps(n int) int {
	steps := 0

	for d := 2; n > 1; d++ {
		for n%d == 0 {
			steps += d
			n /= d
		}
	}

	return steps
}
