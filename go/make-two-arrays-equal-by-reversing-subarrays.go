package main

import "slices"

func canBeEqual(target []int, arr []int) bool {
	slices.Sort(target)
	slices.Sort(arr)

	for i := range target {
		if arr[i] != target[i] {
			return false
		}
	}

	return true
}
