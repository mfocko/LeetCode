package main

func checkSubarraySum(nums []int, k int) bool {
	first_index_of := make(map[int]int)
	first_index_of[0] = -1

	remainder := 0
	for i, x := range nums {
		remainder = (remainder + x) % k

		j, seen := first_index_of[remainder]
		if seen {
			if i-j > 1 {
				return true
			}
		} else {
			first_index_of[remainder] = i
		}
	}

	return false
}
