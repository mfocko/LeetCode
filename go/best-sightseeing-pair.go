package main

func maxScoreSightseeingPair(values []int) int {
	maxLeft := values[0]

	maxScore := 0
	for i := 1; i < len(values); i++ {
		rightScore := values[i] - i
		maxScore = max(maxScore, maxLeft+rightScore)

		maxLeft = max(maxLeft, values[i]+i)
	}

	return maxScore
}
