package main

import (
	arrayqueue "github.com/emirpasic/gods/v2/queues/arrayqueue"
)

func minKBitFlips(bits []int, k int) int {
	q := arrayqueue.New[int]()
	total := 0

	flipped := 0
	for i, bit := range bits {
		if i >= k {
			b, _ := q.Dequeue()
			flipped ^= b
		}

		if flipped != bit {
			q.Enqueue(0)
			continue
		}

		if i+k > len(bits) {
			return -1
		}

		q.Enqueue(1)
		flipped ^= 1
		total++
	}

	return total
}
