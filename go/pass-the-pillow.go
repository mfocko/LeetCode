package main

func passThePillow(n int, time int) int {
	rounds, leftover := time/(n-1), time%(n-1)

	if rounds%2 == 0 {
		return leftover + 1
	}
	return n - leftover
}
