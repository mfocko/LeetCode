package main

func subsets(nums []int) [][]int {
	var sets [][]int

	var subsetsRec func(int)

	var runningSet []int
	subsetsRec = func(i int) {
		if i == len(nums) {
			toAppend := make([]int, len(runningSet))
			copy(toAppend, runningSet)

			sets = append(sets, toAppend)
			return
		}

		// Skip the current element
		subsetsRec(i + 1)

		// Include the current element
		runningSet = append(runningSet, nums[i])
		subsetsRec(i + 1)
		runningSet = runningSet[:len(runningSet)-1]
	}

	subsetsRec(0)
	return sets
}
