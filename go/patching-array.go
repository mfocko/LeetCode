package main

func minPatches(nums []int, n int) int {
	missing := 1
	patches := 0

	i := 0
	for missing <= n {
		if i < len(nums) && nums[i] <= missing {
			missing += nums[i]
			i++
		} else {
			missing += missing
			patches++
		}
	}

	return patches
}
