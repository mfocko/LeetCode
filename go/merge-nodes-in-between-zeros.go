package main

func mergeNodes(head *ListNode) *ListNode {
	head = head.Next
	if head == nil {
		return nil
	}

	node := head.Next
	for node.Val != 0 {
		head.Val += node.Val
		node = node.Next
	}

	head.Next = mergeNodes(node)

	return head
}
