package main

func restoreMatrix(rowSum []int, colSum []int) [][]int {
	rows, cols := len(rowSum), len(colSum)

	matrix := make([][]int, rows)
	for y := range rows {
		matrix[y] = make([]int, cols)
	}

	y, x := 0, 0
	for y < rows && x < cols {
		matrix[y][x] = min(rowSum[y], colSum[x])

		rowSum[y] -= matrix[y][x]
		colSum[x] -= matrix[y][x]

		if rowSum[y] == 0 {
			y++
		} else {
			x++
		}
	}

	return matrix
}
