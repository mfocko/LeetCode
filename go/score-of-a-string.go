package main

func scoreOfString(s string) int {
	abs := func(x int) int {
		return max(x, -x)
	}

	score := 0

	for i := 0; i+1 < len(s); i++ {
		score += abs(int(s[i]) - int(s[i+1]))
	}

	return score
}
