package main

func minDays(bloomDay []int, m int, k int) int {
	getBouquets := func(day int) int {
		bouquets := 0

		count := 0
		for i := range bloomDay {
			if bloomDay[i] <= day {
				count++
			} else {
				count = 0
			}

			if count == k {
				bouquets++
				count = 0
			}
		}

		return bouquets
	}

	firstDay := 0
	lastDay := 0
	for _, day := range bloomDay {
		lastDay = max(lastDay, day)
	}

	minDay := -1
	for firstDay <= lastDay {
		mid := (firstDay + lastDay) / 2

		if getBouquets(mid) >= m {
			minDay = mid
			lastDay = mid - 1
		} else {
			firstDay = mid + 1
		}
	}

	return minDay
}
