package main

func sortColors(nums []int) {
	l, r := 0, len(nums)-1

	for i, _ := range nums {
		for l <= i && i <= r && nums[i] != 1 {
			if nums[i] == 0 {
				nums[i], nums[l] = nums[l], nums[i]
				l++
			} else if nums[i] == 2 {
				nums[i], nums[r] = nums[r], nums[i]
				r--
			}
		}
	}
}
