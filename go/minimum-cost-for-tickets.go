package main

func mincostTickets(days []int, costs []int) int {
	daily, weekly, monthly := costs[0], costs[1], costs[2]

	lastDay := days[len(days)-1]

	dp := make([]int, lastDay+1)
	for i, day := 0, 1; day <= lastDay; day++ {
		if day < days[i] {
			// skip tickets when not travelling
			dp[day] = dp[day-1]
			continue
		}

		dp[day] = min(
			dp[day-1]+daily,
			dp[max(0, day-7)]+weekly,
			dp[max(0, day-30)]+monthly,
		)
		i++
	}

	return dp[lastDay]
}
