package main

import as "github.com/emirpasic/gods/v2/stacks/arraystack"

func asteroidCollision(asteroids []int) []int {
	st := as.New[int]()

	for _, asteroid := range asteroids {
		alive := true

		for top, ok := st.Peek(); ok && asteroid < 0 && top > 0; top, ok = st.Peek() {
			if top < -asteroid {
				st.Pop()
				continue
			} else if top == -asteroid {
				st.Pop()
			}

			alive = false
			break
		}

		if alive {
			st.Push(asteroid)
		}
	}

	result := make([]int, st.Size())
	for i := st.Size() - 1; i >= 0; i-- {
		result[i], _ = st.Pop()
	}
	return result
}
