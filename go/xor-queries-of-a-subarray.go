package main

func xorQueries(arr []int, queries [][]int) []int {
	result := make([]int, len(queries))

	// Prefix XOR
	for i := 1; i < len(arr); i++ {
		arr[i] ^= arr[i-1]
	}

	// Evaluate queries
	for i, query := range queries {
		l, r := query[0], query[1]

		if l > 0 {
			result[i] = arr[l-1] ^ arr[r]
		} else {
			result[i] = arr[r]
		}
	}

	return result
}
