package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_PatchingArray_Example1(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(1, minPatches([]int{1, 3}, 6))
}

func Test_PatchingArray_Example2(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(2, minPatches([]int{1, 5, 10}, 20))
}

func Test_PatchingArray_Example3(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(0, minPatches([]int{1, 2, 2}, 5))
}
