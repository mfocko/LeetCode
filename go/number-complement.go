package main

func findComplement(num int) int {
	mask := 0
	for tmp := num; tmp != 0; tmp >>= 1 {
		mask <<= 1
		mask |= 1
	}

	return num ^ mask
}
