package main

func maxWidthRamp(nums []int) int {
	st := make([]int, 0)

	for i, x := range nums {
		if len(st) == 0 || nums[st[len(st)-1]] > x {
			st = append(st, i)
		}
	}

	widest := 0
	for j := len(nums) - 1; j >= 0; j-- {
		for len(st) > 0 && nums[st[len(st)-1]] <= nums[j] {
			widest = max(widest, j-st[len(st)-1])
			st = st[:len(st)-1]
		}
	}

	return widest
}
