package main

func isSubsequence(s string, t string) bool {
	length := len(s)

	i := 0
	for j := range t {
		if i < length && t[j] == s[i] {
			i++
		}

		if i >= length {
			break
		}
	}

	return i == length
}
