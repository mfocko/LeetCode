package main

func gridGame(grid [][]int) int64 {
	prefix, suffix := int64(0), int64(0)

	// first row gets precomputed separately
	for _, points := range grid[0] {
		prefix += int64(points)
	}

	minSum := int64(10000000001)
	for turn, first := range grid[0] {
		prefix -= int64(first)

		minSum = min(minSum, max(prefix, suffix))
		suffix += int64(grid[1][turn])
	}

	return minSum
}
