package main

func reverseParentheses(s string) string {
	getParentheses := func() []int {
		pairing := make([]int, len(s))

		opened := make([]int, 0, len(s)/2)
		for i, c := range s {
			if c == '(' {
				opened = append(opened, i)
			} else if c == ')' {
				var j int
				j, opened = opened[len(opened)-1], opened[:len(opened)-1]

				pairing[i] = j
				pairing[j] = i
			}
		}

		return pairing
	}

	pairing := getParentheses()

	result := make([]byte, 0, len(s))
	for i, d := 0, 1; i < len(s); i += d {
		if s[i] == '(' || s[i] == ')' {
			i = pairing[i]
			d *= -1
		} else {
			result = append(result, s[i])
		}
	}

	return string(result)
}
