package main

import "strings"

func areSentencesSimilar(sentence1 string, sentence2 string) bool {
	checkSimilar := func(words1, words2 []string) bool {
		left := 0
		for left < len(words1) && words1[left] == words2[left] {
			left++
		}

		right1, right2 := len(words1)-1, len(words2)-1
		for right1 >= 0 && words1[right1] == words2[right2] {
			right1--
			right2--
		}

		return right1 < left
	}

	words1 := strings.Split(sentence1, " ")
	words2 := strings.Split(sentence2, " ")

	if len(words1) > len(words2) {
		words1, words2 = words2, words1
	}

	return checkSimilar(words1, words2)
}
