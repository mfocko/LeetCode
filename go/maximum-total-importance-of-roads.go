package main

import "slices"

func maximumImportance(n int, roads [][]int) int64 {
	degrees := make([]int64, n)

	for _, edge := range roads {
		degrees[edge[0]]++
		degrees[edge[1]]++
	}

	slices.Sort(degrees)

	importance := int64(0)
	for i, degree := range degrees {
		importance += int64(i+1) * degree
	}

	return importance
}
