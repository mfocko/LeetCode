package main

import "slices"

func maxOperations(nums []int, k int) int {
	slices.Sort(nums)

	operations := 0

	l, r := 0, len(nums)-1
	for l < r {
		if nums[l]+nums[r] == k {
			operations++
			l++
			r--
		} else if nums[l]+nums[r] < k {
			l++
		} else {
			r--
		}
	}

	return operations
}
