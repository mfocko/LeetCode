package main

func numberOfSubarrays(nums []int, k int) int {
	atMost := func(k int) int {
		subarrays := 0

		window, start := 0, 0
		for end := range nums {
			window += nums[end] % 2

			for window > k {
				window -= nums[start] % 2
				start++
			}

			subarrays += end - start + 1
		}

		return subarrays
	}

	return atMost(k) - atMost(k-1)
}
