package main

func judgeSquareSum(c int) bool {
	bsearch := func(left, right, target int64) bool {
		for left <= right {
			mid := left + (right-left)/2

			if mid*mid == target {
				return true
			} else if mid*mid > target {
				right = mid - 1
			} else {
				left = mid + 1
			}
		}

		return false
	}

	t := int64(c)
	for a := int64(0); a*a <= t; a++ {
		b := t - a*a
		if bsearch(0, b, b) {
			return true
		}
	}
	return false
}
