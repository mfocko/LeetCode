package main

import (
	"cmp"
	"slices"
	"strconv"
)

func largestNumber(nums []int) string {
	numbers := make([]string, len(nums))
	for i, num := range nums {
		numbers[i] = strconv.Itoa(num)
	}

	slices.SortFunc(numbers, func(a, b string) int {
		return cmp.Compare(b+a, a+b)
	})

	if numbers[0] == "0" {
		return "0"
	}

	largest := ""
	for _, number := range numbers {
		largest += number
	}

	return largest
}
