package main

func maxSatisfied(customers []int, grumpy []int, minutes int) int {
	// count always satisfied customers
	satisfied := 0
	for i, count := range customers {
		satisfied += count * (1 - grumpy[i])
	}

	// get the initial run
	unsatisfied := 0
	for i := 0; i < minutes; i++ {
		unsatisfied += customers[i] * grumpy[i]
	}

	maxCoverage := unsatisfied
	for i := minutes; i < len(customers); i++ {
		// remove from the start
		unsatisfied -= customers[i-minutes] * grumpy[i-minutes]

		// add from the end
		unsatisfied += customers[i] * grumpy[i]

		// update max coverage
		maxCoverage = max(maxCoverage, unsatisfied)
	}

	return maxCoverage + satisfied
}
