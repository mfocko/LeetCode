package main

import "slices"

func vowelStrings(words []string, queries [][]int) []int {
	isVowel := func(c byte) bool {
		return slices.Contains([]byte{'a', 'e', 'i', 'o', 'u'}, c)
	}

	makePrefixCount := func() []int {
		prefixCount := make([]int, len(words))

		count := 0
		for i, word := range words {
			if isVowel(word[0]) && isVowel(word[len(word)-1]) {
				count++
			}
			prefixCount[i] = count
		}

		return prefixCount
	}
	prefixCount := makePrefixCount()

	answer := make([]int, len(queries))
	for i, query := range queries {
		till := prefixCount[query[1]]

		preceding := 0
		if query[0] != 0 {
			preceding = prefixCount[query[0]-1]
		}

		answer[i] = till - preceding
	}

	return answer
}
