package main

func gcdOfStrings(str1 string, str2 string) string {
	gcd := func(x, y int) int {
		for y != 0 {
			x, y = y, x%y
		}
		return x
	}

	if str1+str2 != str2+str1 {
		return ""
	}

	length := gcd(len(str1), len(str2))
	return str1[:length]
}
