package main

import "math"

func increasingTriplet(nums []int) bool {
	a, b := math.MaxInt, math.MaxInt
	for _, x := range nums {
		if x <= a {
			a = x
		} else if x <= b {
			b = x
		} else {
			return true
		}
	}

	return false
}
