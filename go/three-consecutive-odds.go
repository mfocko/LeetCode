package main

func threeConsecutiveOdds(arr []int) bool {
	for i := 0; i+2 < len(arr); i++ {
		if arr[i]&arr[i+1]&arr[i+2]&1 != 0 {
			return true
		}
	}

	return false
}
