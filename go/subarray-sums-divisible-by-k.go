package main

func subarraysDivByK(nums []int, k int) int {
	counters := make([]int, k)
	counters[0] = 1

	total := 0

	runningMod := 0
	for _, num := range nums {
		runningMod = (k + runningMod + num%k) % k

		total += counters[runningMod]
		counters[runningMod]++
	}

	return total
}
