package main

func minimumLength(s string) int {
	freqs := make([]int, 26)
	for _, c := range s {
		freqs[c-'a']++
	}

	length := len(s)
	for _, c := range freqs {
		if c <= 2 {
			// cannot remove anything
			continue
		}

		// middle character must be kept
		c--

		// if there's odd count left, there will be one more character kept
		if c%2 != 0 {
			c--
		}

		// rest can be safely removed
		length -= c
	}

	return length
}
