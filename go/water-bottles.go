package main

func numWaterBottles(numBottles int, numExchange int) int {
	drank := 0

	for numBottles >= numExchange {
		canGet := numBottles / numExchange

		drank += canGet * numExchange
		numBottles -= canGet * numExchange

		numBottles += canGet
	}

	return drank + numBottles
}
