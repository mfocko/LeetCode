/**
 * @param {Function} fn
 * @return {Array}
 */
Array.prototype.groupBy = function(fn) {
    let grouped = new Object();

    for (let x of this) {
        let key = fn(x);

        if (grouped[key] === undefined) {
            grouped[key] = new Array();
        }

        grouped[key].push(x);
    }

    return grouped;
};

/**
 * [1,2,3].groupBy(String) // {"1":[1],"2":[2],"3":[3]}
 */
