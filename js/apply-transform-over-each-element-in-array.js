/**
 * @param {number[]} arr
 * @param {Function} fn
 * @return {number[]}
 */
var map = function(arr, fn) {
    let mapped_arr = new Array();

    let i = 0;
    for (let x of arr) {
        mapped_arr.push(fn(x, i++));
    }

    return mapped_arr;
};
