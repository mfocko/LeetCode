/**
 * @param {number[]} arr
 * @param {Function} fn
 * @return {number[]}
 */
var filter = function(arr, fn) {
    let new_arr = new Array();

    let i = 0;
    for (let x of arr) {
        if (fn(x, i++)) {
            new_arr.push(x);
        }
    }

    return new_arr;
};
