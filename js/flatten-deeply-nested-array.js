/**
 * @param {any[]} arr
 * @param {number} depth
 * @return {any[]}
 */
var flat = function (arr, n) {
    let stack = [[arr, 0, 0]];

    let flattened = [];
    while (stack.length) {
        let [arr, depth, i] = stack.pop();
        if (i >= arr.length) {
            // finished with the nested array
            continue;
        }

        // if it's a nested array and not deep enough
        if (depth < n && arr[i] instanceof Array) {
            // gotta continue with the next element of the array
            stack.push([arr, depth, i + 1]);

            // gotta start with the first element of the nested
            stack.push([arr[i], depth + 1, 0]);

            // yielding is done only on the elements themselves
            continue;
        }

        flattened.push(arr[i]);
        stack.push([arr, depth, i + 1]);
    }

    return flattened;
};
