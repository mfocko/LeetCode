/**
 * @param {Array} arr
 * @return {Generator}
 */
var inorderTraversal = function*(arr) {
    let stack = [[arr, 0]];

    while (stack.length) {
        let [arr, i] = stack.pop();
        if (i >= arr.length) {
            // finished with the nested array
            continue;
        }

        // if it's a nested array
        if (arr[i] instanceof Array) {
            // gotta continue with the next element of the array
            stack.push([arr, i + 1]);

            // gotta start with the first element of the nested
            stack.push([arr[i], 0]);

            // yielding is done only on the elements themselves
            continue;
        }

        yield arr[i];
        stack.push([arr, i + 1]);
    }
};

/**
 * const gen = inorderTraversal([1, [2, 3]]);
 * gen.next().value; // 1
 * gen.next().value; // 2
 * gen.next().value; // 3
 */
