/**
 * @param {Function} fn
 * @param {number} t
 * @return {Function}
 */
var throttle = function(fn, t) {
  let on_cooldown = false;
  let queuedArgs = null;

  const handler = () => {
    if (queuedArgs === null) {
      on_cooldown = false;
      return;
    }

    fn.apply(null, queuedArgs);
    queuedArgs = null;
    on_cooldown = true;

    setTimeout(handler, t);
  };

  return function(...args) {
      queuedArgs = args;
      if (!on_cooldown) {
          handler();
      }
  }
};

/**
 * const throttled = throttle(console.log, 100);
 * throttled("log"); // logged immediately.
 * throttled("log"); // logged at t=100ms.
 */
