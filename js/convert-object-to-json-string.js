/**
 * @param {any} object
 * @return {string}
 */
var jsonStringify = function(object) {
    switch (typeof object) {
        case 'boolean':
        case 'number':
            return `${object}`;
        case 'string':
            return `"${object}"`;
        case 'object':
            if (Array.isArray(object)) {
                return `[${object.map(jsonStringify).join(',')}]`;
            }

            if (object) {
                let nested = Object.keys(object).map(key => `"${key}":${jsonStringify(object[key])}`);
                return `{${nested.join(',')}}`;
            }
            return 'null';
        default:
            return '';
    }
};
