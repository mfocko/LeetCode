#include <cassert>
#include <vector>

class Solution {
  public:
    std::vector<int> dailyTemperatures(const std::vector<int> &temperatures) {
        std::vector<int> result(temperatures.size(), 0);
        std::vector<int> st;

        for (auto i = 0u; i < temperatures.size(); ++i) {
            while (!st.empty() && temperatures[st.back()] < temperatures[i]) {
                result[st.back()] = i - st.back();
                st.pop_back();
            }

            st.push_back(i);
        }

        return result;
    }
};

int main() {
    Solution s;

    assert((s.dailyTemperatures(std::vector{73, 74, 75, 71, 69, 72, 76, 73}) ==
            std::vector{1, 1, 4, 2, 1, 1, 0, 0}));
    assert((s.dailyTemperatures(std::vector{30, 40, 50, 60}) ==
            std::vector{1, 1, 1, 0}));
    assert(
        (s.dailyTemperatures(std::vector{30, 60, 90}) == std::vector{1, 1, 0}));

    return 0;
}
