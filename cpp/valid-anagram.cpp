#include <algorithm>
#include <array>
#include <string>

class Solution {
  public:
    bool isAnagram(const std::string &s, const std::string &t) {
        std::array<int, 26> counter{};

        for (char c : s) {
            counter[c - 'a']++;
        }

        for (char c : t) {
            counter[c - 'a']--;

            if (counter[c - 'a'] < 0) {
                return false;
            }
        }

        return std::all_of(counter.begin(), counter.end(),
                           [](auto c) { return c == 0; });
    }
};
