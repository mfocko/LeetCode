#include <cassert>
#include <string>

class Solution {
  public:
    std::string mergeAlternately(const std::string &word1,
                                 const std::string &word2) {
        std::string result;

        auto l = word1.begin();
        auto r = word2.begin();

        for (; l != word1.end() && r != word2.end(); ++l, ++r) {
            result += *l;
            result += *r;
        }

        for (; l != word1.end(); ++l) {
            result += *l;
        }

        for (; r != word2.end(); ++r) {
            result += *r;
        }

        return result;
    }
};

int main() {
    Solution s;

    assert(s.mergeAlternately("abc", "pqr") == "apbqcr");
    assert(s.mergeAlternately("ab", "pqrs") == "apbqrs");
    assert(s.mergeAlternately("abcd", "pq") == "apbqcd");

    return 0;
}
