#include <string>
#include <vector>

class Solution {
  public:
    std::string removeStars(const std::string &s) {
        std::vector<char> without_stars;

        for (auto c : s) {
            if (c == '*') {
                without_stars.pop_back();
            } else {
                without_stars.push_back(c);
            }
        }

        return std::string{without_stars.begin(), without_stars.end()};
    }
};
