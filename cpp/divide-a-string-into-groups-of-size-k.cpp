#include <string>
#include <vector>

using std::string;
using std::vector;

class Solution {
  public:
    vector<string> divideString(string s, int k, char fill) {
        vector<string> result;

        for (int i = 0; i < s.size(); i += k) {
            result.push_back(s.substr(i, k));
        }

        auto size_of_last = result.back().size();
        if (size_of_last < k) {
            result.back().append(k - size_of_last, fill);
        }

        return result;
    }
};
