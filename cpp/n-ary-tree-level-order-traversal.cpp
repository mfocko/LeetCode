/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() { }

    Node(int _val)
    {
        val = _val;
    }

    Node(int _val, vector<Node*> _children)
    {
        val = _val;
        children = _children;
    }
};
*/

class Solution {
    void levelOrder(vector<vector<int>> &traversal, Node *root, int level) {
        if (root == nullptr) {
            return;
        }

        if (level >= traversal.size()) {
            traversal.push_back({});
        }
        traversal[level].push_back(root->val);

        for (auto child : root->children) {
            levelOrder(traversal, child, level + 1);
        }
    }

  public:
    vector<vector<int>> levelOrder(Node *root) {
        vector<vector<int>> result;
        levelOrder(result, root, 0);
        return result;
    }
};
