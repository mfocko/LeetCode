class Solution {
    int right(const std::vector<int> &row, int size, int idx) const {
        if (idx >= size) {
            return 0;
        }
        return row[idx];
    }

  public:
    int uniquePaths(int m, int n) {
        std::vector<int> bottom(n, 1);

        for (int y = m - 2; y >= 0; y--) {
            std::vector<int> above(n, 0);

            for (int x = n - 1; x >= 0; x--) {
                above[x] = right(above, n, x + 1) + bottom[x];
            }

            bottom = std::move(above);
        }

        return bottom[0];
    }
};
