#include <cassert>
#include <vector>

class Solution {
    constexpr static int MOD = 1000000007;

    static void add_with_mod(std::vector<int> &good, std::size_t i, int value) {
        good[i] = (good[i] + value) % MOD;
    }

  public:
    int countGoodStrings(int low, int high, int zero, int one) {
        assert(low <= high);

        std::vector<int> good(high + 1, 0);
        good[0] = 1;

        for (int length = 1; length <= high; ++length) {
            if (length >= zero) {
                add_with_mod(good, length, good[length - zero]);
            }

            if (length >= one) {
                add_with_mod(good, length, good[length - one]);
            }
        }

        int total = 0;
        for (int i = low; i <= high; ++i) {
            total = (total + good[i]) % MOD;
        }

        return total;
    }
};

int main() {
    Solution s;

    assert(s.countGoodStrings(3, 3, 1, 1) == 8);
    assert(s.countGoodStrings(2, 3, 1, 2) == 5);

    return 0;
}
