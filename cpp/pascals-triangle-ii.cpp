class Solution {
  public:
    vector<int> getRow(int rowIndex) {
        vector<int> result;

        result.push_back(1);
        for (auto k = 0; k < rowIndex; k++) {
            auto next = static_cast<int>(static_cast<long>(result.back()) *
                                         (rowIndex - k) / (k + 1));
            result.push_back(next);
        }

        return result;
    }
};
