#include <algorithm>
#include <vector>

class Solution {
  public:
    int findContentChildren(std::vector<int> g, std::vector<int> s) {
        std::sort(g.begin(), g.end());
        std::sort(s.begin(), s.end());

        int content = 0;
        for (int i = 0, j = 0; i < g.size() && j < s.size(); ++j) {
            if (g[i] <= s[j]) {
                ++i;
                ++content;
            }
        }

        return content;
    }
};
