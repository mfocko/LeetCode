class Solution {
    static int get(const std::vector<int> &nums, int i) {
        if (i < 0 || i >= static_cast<int>(nums.size())) {
            return 0;
        }

        return nums[i];
    }

  public:
    int rob(std::vector<int> nums) {
        if (nums.size() == 0) {
            return 0;
        }

        for (int i = 1; i < static_cast<int>(nums.size()); ++i) {
            nums[i] = std::max(nums[i - 1], get(nums, i - 2) + nums[i]);
        }

        return nums.back();
    }
};
