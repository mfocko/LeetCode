#include <cstdint>
#include <string>
#include <unordered_map>

class Solution {
    static const std::uint32_t BIT = 1;

  public:
    long long wonderfulSubstrings(const std::string &word) {
        long long result = 0;

        std::unordered_map<std::uint32_t, long long> freqs;
        freqs[0] = 1;

        std::uint32_t mask = 0;
        for (char c : word) {
            mask ^= BIT << (c - 'a');

            result += freqs[mask];
            ++freqs[mask];

            for (auto odd_c = 0; odd_c < 10; ++odd_c) {
                result += freqs[mask ^ (BIT << odd_c)];
            }
        }

        return result;
    }
};
