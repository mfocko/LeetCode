#ifdef _MF_TEST
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};
#endif

class Solution {
  public:
    ListNode *mergeInBetween(ListNode *list1, int a, int b, ListNode *list2) {
        auto left = list1;
        for (int i = 1; i < a; ++i) {
            left = left->next;
        }

        auto right = left;
        for (int i = a; i < b + 2; ++i) {
            right = right->next;
        }

        left->next = list2;

        auto last_mid = list2;
        while (last_mid->next != nullptr) {
            last_mid = last_mid->next;
        }
        last_mid->next = right;

        return list1;
    }
};
