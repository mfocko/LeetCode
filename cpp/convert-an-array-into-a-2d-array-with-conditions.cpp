#include <map>
#include <vector>

class Solution {
  public:
    std::vector<std::vector<int>> findMatrix(const std::vector<int> &nums) {
        // count the numbers
        std::map<int, int> freqs;
        for (auto x : nums) {
            ++freqs[x];
        }

        std::vector<std::vector<int>> matrix;

        bool elements_left = true;
        while (elements_left) {
            elements_left = false;

            std::vector<int> row;
            for (auto &[num, count] : freqs) {
                if (count <= 0) {
                    continue;
                }

                --count;
                row.push_back(num);
                elements_left = elements_left || count > 0;
            }
            matrix.push_back(row);
        }

        return matrix;
    }
};
