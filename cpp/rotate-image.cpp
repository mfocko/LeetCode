#include <algorithm>
#include <vector>

class Solution {
  public:
    void rotate(std::vector<std::vector<int>> &matrix) {
        for (std::size_t i = 0; i < matrix.size(); i++) {
            for (std::size_t j = i; j < matrix.size(); j++) {
                std::swap(matrix[i][j], matrix[j][i]);
            }
        }

        for (auto &row : matrix) {
            std::reverse(row.begin(), row.end());
        }
    }
};
