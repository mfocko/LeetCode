#include <string>
#include <vector>

class Solution {
  public:
    bool isValid(const std::string &s) {
        std::vector<char> st;

        for (auto c : s) {
            switch (c) {
            case '(':
                st.push_back(')');
                break;
            case '{':
                st.push_back('}');
                break;
            case '[':
                st.push_back(']');
                break;
            default:
                if (st.empty() || st.back() != c) {
                    return false;
                }
                st.pop_back();
                break;
            }
        }

        return st.empty();
    }
};
