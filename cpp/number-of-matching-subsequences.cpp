class Solution {
    bool matches(const string &s, const string &word) const {
        auto s_i = 0;

        for (auto i = 0; s_i < s.size() && i < word.size(); i++) {
            if (s[s_i] == word[i]) {
                s_i++;
            }
        }

        return s_i == s.size();
    }

    map<string, int> preprocess(const vector<string> &words) const {
        map<string, int> histogram;

        for (auto &w : words) {
            histogram[w]++;
        }

        return histogram;
    }

  public:
    int numMatchingSubseq(string s, vector<string> &words) {
        auto histogram = preprocess(words);
        return accumulate(histogram.begin(), histogram.end(), 0,
                          [&](int acc, const auto &pair) {
                              return acc +
                                     (matches(pair.first, s) ? pair.second : 0);
                          });
    }
};
