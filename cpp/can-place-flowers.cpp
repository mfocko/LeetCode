#include <cassert>
#include <vector>

using std::vector;

class Solution {
  public:
    bool canPlaceFlowers(vector<int> &flowerbed, int n) {
        int count = 0;

        int left = 0, right;
        for (vector<int>::size_type i = 0; i < flowerbed.size(); i++) {
            right = (i + 1 < flowerbed.size()) ? flowerbed[i + 1] : 0;

            if (left == 0 && flowerbed[i] == 0 && right == 0) {
                count++;
                left = 1;
            } else {
                left = flowerbed[i];
            }
        }

        return count >= n;
    }
};

int main() {
    Solution s;

    std::vector flowers{1, 0, 0, 0, 1};
    assert(s.canPlaceFlowers(flowers, 1));
    assert(!s.canPlaceFlowers(flowers, 2));

    flowers = {1, 0, 0, 0, 0, 1};
    assert(!s.canPlaceFlowers(flowers, 2));

    flowers = {1, 0, 0, 0, 1, 0, 0};
    assert(s.canPlaceFlowers(flowers, 2));

    flowers = {0, 0, 1, 0, 0};
    assert(s.canPlaceFlowers(flowers, 1));

    return 0;
}
