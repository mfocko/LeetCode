#include <algorithm>
#include <string>
#include <unordered_map>
#include <vector>

class Solution {
    using words_t = std::vector<std::string>;

  public:
    std::vector<words_t> groupAnagrams(const words_t &strs) {
        std::unordered_map<std::string, words_t> groups;
        for (const auto &s : strs) {
            std::string sorted_s = s;
            std::sort(sorted_s.begin(), sorted_s.end());

            groups[sorted_s].push_back(s);
        }

        std::vector<words_t> result;
        for (const auto [_, words] : groups) {
            result.push_back(std::move(words));
        }

        return result;
    }
};
