#include <cassert>
#include <functional>
#include <vector>

using std::vector;

class Solution {
    struct Question {
        long long answered;
        long long skipped;

        long long get() const { return std::max(answered, skipped); }
    };

    long long getWithOffset(vector<Question> &questions, int offset) {
        auto i = questions.size() - offset - 1;
        if (i < 0 || i >= questions.size()) {
            return 0;
        }

        return questions[i].get();
    }

  public:
    long long mostPoints(vector<vector<int>> &questions) {
        vector<Question> stack;

        std::for_each(questions.rbegin(), questions.rend(), [&](auto &q_in) {
            Question q;
            q.answered = q_in[0] + getWithOffset(stack, q_in[1]);
            q.skipped = getWithOffset(stack, 0);

            stack.push_back(q);
        });

        return stack.back().get();
    }
};

int main() {
    Solution s;
    vector<vector<int>> questions;

    questions = {{3, 2}, {4, 3}, {4, 4}, {2, 5}};
    assert(s.mostPoints(questions) == 5);

    questions = {{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}};
    assert(s.mostPoints(questions) == 7);

    return 0;
}
