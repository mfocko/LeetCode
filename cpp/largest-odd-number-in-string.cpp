#include <string>

class Solution {
  public:
    std::string largestOddNumber(const std::string &num) {
        auto i = num.find_last_of("13579");
        if (i == std::string::npos) {
            return "";
        }

        return num.substr(0, i + 1);
    }
};
