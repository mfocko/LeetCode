/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left),
 * right(right) {}
 * };
 */
class Solution {
    int goodNodes(TreeNode *root, int m) {
        if (root == nullptr) {
            return 0;
        }

        int new_max = std::max(m, root->val);
        return (root->val >= m) + goodNodes(root->left, new_max) +
               goodNodes(root->right, new_max);
    }

  public:
    int goodNodes(TreeNode *root) { return goodNodes(root, INT_MIN); }
};
