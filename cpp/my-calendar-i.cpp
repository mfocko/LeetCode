#include <cassert>
#include <iostream>
#include <vector>

namespace {

struct range_t {
    int start;
    int end;

    range_t(int start, int end) : start(start), end(end) {}

    bool has(int x) const { return x > start && x < end; }

    bool operator>(const range_t &other) const { return start > other.start; }

    bool operator==(const range_t &other) const {
        return start == other.start && end == other.end;
    }
};

bool overlaps(const range_t &a, const range_t &b) {
    return a == b || a.has(b.start) || a.has(b.end) || b.has(a.start) ||
           b.has(a.end);
}

} // namespace

class MyCalendar {
    std::vector<range_t> entries;

  public:
    MyCalendar() = default;

    bool book(int start, int end) {
        entries.emplace_back(start, end);

        auto it = entries.end();
        it--;

        for (int i = entries.size() - 1; i > 0 && entries[i - 1] > entries[i];
             i--, it--) {
            if (overlaps(entries[i], entries[i - 1])) {
                entries.erase(it);
                return false;
            }

            std::swap(entries[i], entries[i - 1]);
        }

        if (it > entries.begin() && overlaps(*it, *(it - 1))) {
            entries.erase(it);
            return false;
        }

        return true;
    }
};

/**
 * Your MyCalendar object will be instantiated and called as such:
 * MyCalendar* obj = new MyCalendar();
 * bool param_1 = obj->book(start,end);
 */

static void test_case_1() {
    std::cout << "=== TEST CASE 1 ===\n";

    std::vector<range_t> entries{{47, 50}, {33, 41}, {39, 45}, {33, 42},
                                 {25, 32}, {26, 35}, {19, 25}, {3, 8},
                                 {8, 13},  {18, 27}};
    std::vector<bool> expected{true,  true, false, false, true,
                               false, true, true,  true,  false};

    MyCalendar c;
    for (auto i = 0; i < entries.size(); i++) {
        std::cout << "[TEST] Entry #" << i << "\n";

        auto [start, end] = entries[i];
        assert(c.book(start, end) == expected[i]);

        std::cout << "[PASS]\n\n";
    }
}

static void test_case_2() {
    std::cout << "=== TEST CASE 2 ===\n";

    std::vector<range_t> entries{
        {37, 50}, {33, 50}, {4, 17}, {35, 48}, {8, 25}};
    std::vector<bool> expected{true, false, true, false, false};

    MyCalendar c;
    for (auto i = 0; i < entries.size(); i++) {
        std::cout << "[TEST] Entry #" << i << "\n";

        auto [start, end] = entries[i];
        assert(c.book(start, end) == expected[i]);

        std::cout << "[PASS]\n\n";
    }
}

static void test_case_3() {
    std::cout << "=== TEST CASE 3 ===\n";

    std::vector<range_t> entries{
        {20, 29}, {13, 22}, {44, 50}, {1, 7},   {2, 10},  {14, 20},
        {19, 25}, {36, 42}, {45, 50}, {47, 50}, {39, 45}, {44, 50},
        {16, 25}, {45, 50}, {45, 50}, {12, 20}, {21, 29}, {11, 20},
        {12, 17}, {34, 40}, {10, 18}, {38, 44}, {23, 32}, {38, 44},
        {15, 20}, {27, 33}, {34, 42}, {44, 50}, {35, 40}, {24, 31}};
    std::vector<bool> expected{
        true,  false, true,  true,  false, true,  false, true,  false, false,
        false, false, false, false, false, false, false, false, false, false,
        false, false, false, false, false, false, false, false, false, false};

    MyCalendar c;
    for (auto i = 0; i < entries.size(); i++) {
        std::cout << "[TEST] Entry #" << i << "\n";

        auto [start, end] = entries[i];
        assert(c.book(start, end) == expected[i]);

        std::cout << "[PASS]\n\n";
    }
}

int main() {
    MyCalendar c;
    assert(c.book(10, 20));
    assert(!c.book(15, 25));
    assert(c.book(20, 30));

    test_case_1();
    test_case_2();
    test_case_3();

    return 0;
}
