#include <cassert>

namespace {
long sum(long index, long value, long n) {
    long count = 0;

    if (value > index) {
        count += (2 * value - index) * (index + 1) / 2;
    } else {
        count += (value + 1) * value / 2 + index - value + 1;
    }

    if (value >= n - index) {
        count += (2 * value - n + 1 + index) * (n - index) / 2;
    } else {
        count += (value + 1) * value / 2 + n - index - value;
    }

    return count - value;
}
} // namespace

class Solution {
  public:
    int maxValue(int n, int index, int maxSum) {
        int left = 1, right = maxSum;

        while (left < right) {
            int mid = (left + right + 1) / 2;
            if (sum(index, mid, n) <= maxSum) {
                left = mid;
            } else {
                right = mid - 1;
            }
        }

        return left;
    }
};

int main() {
    Solution s;

    assert(s.maxValue(4, 2, 6) == 2);
    assert(s.maxValue(6, 1, 10) == 3);

    // regression
    assert(s.maxValue(6, 2, 931384943) == 155230825);

    return 0;
}
