#include <algorithm>
#include <set>

class Solution {
  public:
    int maxWidthOfVerticalArea(const std::vector<std::vector<int>> &points) {
        std::set<int> seen;
        for (const auto &point : points) {
            seen.insert(point[0]);
        }

        int last = *seen.begin();
        int max_width = 0;
        for (const auto x : seen) {
            max_width = std::max(max_width, x - last);
            last = x;
        }

        return max_width;
    }
};
