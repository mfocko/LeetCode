#include <cassert>
#include <vector>

class Solution {
    template <typename C> struct arr_iterator {
        using It = typename C::const_iterator;

        It it, end;

        arr_iterator(const C &container)
            : it(container.begin()), end(container.end()) {}

        void init() {
            while (it != end && !predicate(*it)) {
                ++it;
            }
        }

        auto operator!=(const It &other_it) const -> bool {
            return it != other_it;
        }

        auto operator*() const -> int { return *it; }

        void operator++() {
            do {
                ++it;
            } while (it != end && !predicate(*it));
        }

        virtual auto predicate(int x) const -> bool = 0;
        virtual ~arr_iterator() {}
    };

    template <typename C> struct pos_iterator : arr_iterator<C> {
        pos_iterator(const C &container) : arr_iterator<C>(container) {
            arr_iterator<C>::init();
        }
        auto predicate(int x) const -> bool override { return x > 0; }
    };

    template <typename C> struct neg_iterator : arr_iterator<C> {
        neg_iterator(const C &container) : arr_iterator<C>(container) {
            arr_iterator<C>::init();
        }
        auto predicate(int x) const -> bool override { return x < 0; }
    };

  public:
    std::vector<int> rearrangeArray(const std::vector<int> &nums) {
        std::vector<int> result;

        auto positive = pos_iterator(nums);
        auto negative = neg_iterator(nums);

        for (bool finished = false; !finished;) {
            finished = true;

            if (positive != nums.end()) {
                result.push_back(*positive);

                ++positive;
                finished = false;
            }

            if (negative != nums.end()) {
                result.push_back(*negative);

                ++negative;
                finished = false;
            }
        }

        return result;
    }
};

int main() {
    Solution s;

    assert((s.rearrangeArray(std::vector{3, 1, -2, -5, 2, -4}) ==
            std::vector{3, -2, 1, -5, 2, -4}));
    assert((s.rearrangeArray(std::vector{-1, 1}) == std::vector{1, -1}));

    return 0;
}
