class Solution {
  public:
    int mirrorReflection(int p, int q) {
        auto lcm = p * q / std::gcd(p, q);
        auto x = lcm / p;
        auto y = lcm / q;

        if (x % 2 == 0) {
            return 0;
        }

        if (y % 2 == 0) {
            return 2;
        }
        return 1;
    }
};
