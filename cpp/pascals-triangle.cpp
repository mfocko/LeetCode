class Solution {
    int getN(const vector<int> &previousRow, int n) {
        if (n == 0 || n == previousRow.size()) {
            return 1;
        }
        return previousRow[n - 1] + previousRow[n];
    }

  public:
    vector<vector<int>> generate(int numRows) {
        if (numRows <= 0) {
            return {};
        }

        vector<vector<int>> result{vector<int>{1}};
        for (auto i = 2; i <= numRows; i++) {
            auto &previous = result.back();
            vector<int> current;

            for (auto j = 0; j < i; j++) {
                current.push_back(getN(previous, j));
            }
            result.push_back(move(current));
        }

        return result;
    }
};
