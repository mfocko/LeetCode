#include <cassert>

class Solution {
  public:
    int minMoves(int target, int maxDoubles) {
        int moves;
        for (moves = 0; target > 1 && maxDoubles > 0; moves++) {
            if (target % 2 == 0) {
                maxDoubles--;
                target /= 2;
            } else {
                target--;
            }
        }

        // remaining increments
        moves += target - 1;

        return moves;
    }
};

int main() {
    Solution s;

    assert(s.minMoves(5, 0) == 4);
    assert(s.minMoves(19, 2) == 7);
    assert(s.minMoves(10, 4) == 4);
    assert(s.minMoves(766972377, 92));
    assert(s.minMoves(1000000000, 5));

    return 0;
}
