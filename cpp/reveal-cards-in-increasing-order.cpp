#include <algorithm>
#include <vector>

class Solution {
  public:
    std::vector<int> deckRevealedIncreasing(std::vector<int> deck) {
        std::vector<int> order(deck.size());

        std::sort(deck.begin(), deck.end());

        bool skip = false;
        for (auto i = 0u, j = 0u; i < deck.size(); j = (j + 1) % deck.size()) {
            if (order[j] == 0) {
                if (!skip) {
                    order[j] = deck[i];
                    ++i;
                }

                skip = !skip;
            }
        }

        return order;
    }
};

#ifdef _MF_TEST
#include <gtest/gtest.h>

TEST(examples, no_1) {
    Solution s;
    EXPECT_EQ((s.deckRevealedIncreasing(std::vector{17, 13, 11, 2, 3, 5, 7})),
              (std::vector{2, 13, 3, 11, 5, 17, 7}));
}
TEST(examples, no_2) {
    Solution s;
    EXPECT_EQ((s.deckRevealedIncreasing(std::vector{1, 1000})),
              (std::vector{1, 1000}));
}
TEST(examples, no_3) {
    Solution s;
    EXPECT_EQ((s.deckRevealedIncreasing(std::vector{1, 2, 3})),
              (std::vector{1, 3, 2}));
}
TEST(examples, no_4) {
    Solution s;
    EXPECT_EQ((s.deckRevealedIncreasing(std::vector{1, 2, 3, 6})),
              (std::vector{1, 3, 2, 6}));
}
TEST(examples, no_5) {
    Solution s;
    EXPECT_EQ((s.deckRevealedIncreasing(std::vector{1, 2, 3, 6, 4})),
              (std::vector{1, 6, 2, 4, 3}));
}
TEST(examples, no_6) {
    Solution s;
    EXPECT_EQ((s.deckRevealedIncreasing(std::vector{1, 2, 3, 6, 4, 10})),
              (std::vector{1, 4, 2, 10, 3, 6}));
}
TEST(examples, no_7) {
    Solution s;
    EXPECT_EQ(
        (s.deckRevealedIncreasing(std::vector{1, 2, 3, 6, 4, 42, 15, 69})),
        (std::vector{1, 6, 2, 42, 3, 15, 4, 69}));
}
TEST(examples, no_8) {
    Solution s;
    EXPECT_EQ(
        (s.deckRevealedIncreasing(std::vector{1, 2, 3, 6, 4, 10, 15, 42})),
        (std::vector{1, 6, 2, 15, 3, 10, 4, 42}));
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
#endif
