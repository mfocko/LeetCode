#include <map>
#include <vector>

class Solution {
  public:
    int minOperations(const std::vector<int> &nums) {
        std::map<int, int> freqs;
        for (const auto &x : nums) {
            ++freqs[x];
        }

        int operations = 0;

        for (const auto &[_, count] : freqs) {
            if (count == 1) {
                return -1;
            }

            switch (count % 3) {
            case 0:
                operations += count / 3;
                break;
            case 1:
            case 2:
                operations += count / 3 + 1;
                break;
            }
        }

        return operations;
    }
};
