class Solution {
  public:
    int totalMoney(int n) {
        auto monday = 1;

        int total = 0;
        for (; n >= 7; ++monday, n -= 7) {
            total += 7 * (2 * monday + 7 - 1) / 2;
        }

        total += n * (monday + monday + n - 1) / 2;

        return total;
    }
};
