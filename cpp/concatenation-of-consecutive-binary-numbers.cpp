#include <cassert>

class Solution {
    static const int MOD = 1000000007;

  public:
    int concatenatedBinary(int n) {
        long joined_number = 0;

        int padding = 1;
        for (int i = 1; i <= n; i++) {
            joined_number = ((joined_number << padding) + i) % MOD;

            if ((i & (i + 1)) == 0) {
                padding++;
            }
        }

        return joined_number;
    }
};

int main() {
    Solution s;

    assert(s.concatenatedBinary(1) == 1);
    assert(s.concatenatedBinary(2) == 6);
    assert(s.concatenatedBinary(3) == 27);
    assert(s.concatenatedBinary(12) == 505379714);

    return 0;
}
