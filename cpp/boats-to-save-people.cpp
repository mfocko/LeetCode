#include <algorithm>
#include <vector>

class Solution {
  public:
    int numRescueBoats(std::vector<int> people, int limit) {
        std::sort(people.begin(), people.end());

        int counter = 0;

        std::size_t left = 0, right = people.size() - 1;
        while (left <= right && left < people.size() && right < people.size()) {
            counter++;

            // if they both it, shift the left pointer
            if (left < right && people[left] + people[right] <= limit) {
                left++;
            }

            // anyways, at least the heaviest person is included
            right--;
        }

        return counter;
    }
};
