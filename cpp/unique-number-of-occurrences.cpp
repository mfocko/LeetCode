#include <map>
#include <set>
#include <vector>

class Solution {
  public:
    bool uniqueOccurrences(const std::vector<int> &arr) {
        std::map<int, std::size_t> freqs;
        for (const auto &x : arr) {
            ++freqs[x];
        }

        // get unique values
        std::set<std::size_t> unique_counts;
        for (const auto &[key, count] : freqs) {
            unique_counts.insert(count);
        }

        return freqs.size() == unique_counts.size();
    }
};
