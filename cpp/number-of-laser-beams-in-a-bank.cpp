#include <algorithm>
#include <string>
#include <vector>

class Solution {
  public:
    int numberOfBeams(const std::vector<std::string> &bank) {
        int beams = 0;

        int last_row = 0;
        for (const auto &row : bank) {
            if (auto current_row = std::count(row.begin(), row.end(), '1');
                current_row != 0) {
                beams += last_row * current_row;
                last_row = current_row;
            }
        }

        return beams;
    }
};
