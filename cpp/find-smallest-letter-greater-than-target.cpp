#include <algorithm>
#include <cassert>
#include <vector>

class Solution {
  public:
    char nextGreatestLetter(const std::vector<char> &letters, char target) {
        auto it = std::lower_bound(letters.begin(), letters.end(), target + 1);
        return it == letters.end() ? letters.front() : *it;
    }
};

int main() {
    Solution s;

    assert((s.nextGreatestLetter(std::vector{'c', 'f', 'j'}, 'a') == 'c'));
    assert((s.nextGreatestLetter(std::vector{'c', 'f', 'j'}, 'c') == 'f'));
    assert((s.nextGreatestLetter(std::vector{'x', 'x', 'y', 'y'}, 'z') == 'x'));

    return 0;
}
