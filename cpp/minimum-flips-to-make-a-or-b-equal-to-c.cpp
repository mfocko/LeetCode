#include <cassert>
#include <tuple>

class Solution {
  public:
    int minFlips(int a, int b, int c) {
        auto flips = 0;

        for (; (a | b) != c; a >>= 1, b >>= 1, c >>= 1) {
            auto [aa, bb, cc] = std::tuple{a & 1, b & 1, c & 1};

            if ((aa | bb) == cc) {
                continue;
            }

            if (aa && bb) {
                flips += 2;
            } else {
                flips++;
            }
        }

        return flips;
    }
};

int main() {
    Solution s;

    assert(s.minFlips(2, 6, 5) == 3);
    assert(s.minFlips(4, 2, 7) == 1);
    assert(s.minFlips(1, 2, 3) == 0);
    assert(s.minFlips(8, 3, 5) == 3); // 1000 | 0011 = 0101

    return 0;
}
