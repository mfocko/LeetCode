#include <cassert>
#include <queue>

class RecentCounter {
    std::queue<int> calls;

  public:
    RecentCounter() {}

    int ping(int t) {
        calls.push(t);

        while (calls.front() < t - 3000) {
            calls.pop();
        }

        return static_cast<int>(calls.size());
    }
};

/**
 * Your RecentCounter object will be instantiated and called as such:
 * RecentCounter* obj = new RecentCounter();
 * int param_1 = obj->ping(t);
 */

static void test_1() {
    RecentCounter c;

    assert(c.ping(1) == 1);
    assert(c.ping(100) == 2);
    assert(c.ping(3001) == 3);
    assert(c.ping(3002) == 3);
}

int main() {
    test_1();
    return 0;
}
