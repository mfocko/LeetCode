#include <cassert>

class ParkingSystem {
    int big;
    int medium;
    int small;

    int &get(int carType) {
        switch (carType) {
        case 1:
            return big;
        case 2:
            return medium;
        case 3:
            return small;
        default:
            assert(false);
        }
    }

  public:
    ParkingSystem(int big, int medium, int small)
        : big(big), medium(medium), small(small) {}

    bool addCar(int carType) {
        auto &space = get(carType);
        if (space <= 0) {
            return false;
        }

        --space;
        return true;
    }
};

/**
 * Your ParkingSystem object will be instantiated and called as such:
 * ParkingSystem* obj = new ParkingSystem(big, medium, small);
 * bool param_1 = obj->addCar(carType);
 */
