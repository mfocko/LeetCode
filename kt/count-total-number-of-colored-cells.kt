class Solution {
    fun coloredCells(n: Int): Long = 1L + 2L * n * (n - 1)
}
