class Solution {
    fun recoverFromPreorder(traversal: String): TreeNode? {
        val levels = mutableListOf<TreeNode>()

        var index = 0
        while (index < traversal.length) {
            var depth = 0
            while (index < traversal.length && traversal[index] == '-') {
                ++depth
                ++index
            }

            var value = 0
            while (index < traversal.length && traversal[index].isDigit()) {
                value = 10 * value + (traversal[index] - '0')
                ++index
            }

            val node = TreeNode(value)
            if (depth < levels.size) {
                levels[depth] = node
            } else {
                levels.add(node)
            }

            if (depth > 0) {
                val parent = levels[depth - 1]
                when {
                    parent.left == null -> parent.left = node
                    else -> parent.right = node
                }
            }
        }

        return levels[0]
    }
}
