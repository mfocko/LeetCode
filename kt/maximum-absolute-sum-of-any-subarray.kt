class Solution {
    private data class Acc(val sum: Int, val min: Int, val max: Int) {
        val maxAbsoluteSum: Int = max - min

        constructor() : this(0, 0, 0) {}

        fun update(num: Int): Acc =
            this.copy(
                sum = sum + num,
                min = listOf(min, sum + num).min()!!,
                max = listOf(max, sum + num).max()!!,
            )
    }

    fun maxAbsoluteSum(nums: IntArray): Int =
        nums.fold(Acc()) { acc, it ->
            acc.update(it)
        }.maxAbsoluteSum
}
