class Solution {
    fun tree2str(root: TreeNode?): String {
        if (root == null) {
            return ""
        }

        val value = root.`val`.toString()
        val left = "(${tree2str(root.left)})"
        val right = "(${tree2str(root.right)})"

        if (left == "()" && right == "()") {
            return value
        } else if (right == "()") {
            return "$value$left"
        }

        return "$value$left$right"
    }
}
