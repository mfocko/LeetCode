class Solution {
    fun minTimeToVisitAllPoints(points: Array<IntArray>): Int =
        points.zip(points.drop(1)).sumOf { (a, b) ->
            val (ax, ay) = a
            val (bx, by) = b
            Math.max((ax - bx).absoluteValue, (ay - by).absoluteValue)
        }
}
