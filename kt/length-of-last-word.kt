class Solution {
    fun lengthOfLastWord(s: String): Int = s.trim().split(Regex("\\s+")).last().length
}
