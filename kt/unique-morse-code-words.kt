class Solution {
    val mapping =
        arrayOf(
            ".-",
            "-...",
            "-.-.",
            "-..",
            ".",
            "..-.",
            "--.",
            "....",
            "..",
            ".---",
            "-.-",
            ".-..",
            "--",
            "-.",
            "---",
            ".--.",
            "--.-",
            ".-.",
            "...",
            "-",
            "..-",
            "...-",
            ".--",
            "-..-",
            "-.--",
            "--..",
        )

    private fun charToMorse(c: Char): String = mapping[c - 'a']

    fun uniqueMorseRepresentations(words: Array<String>): Int =
        words
            .map { word ->
                word.map { charToMorse(it) }.joinToString(separator = "")
            }
            .toSet()
            .size
}

fun main() {
    val s = Solution()

    check(s.uniqueMorseRepresentations(arrayOf("gin", "zen", "gig", "msg")) == 2)
    check(s.uniqueMorseRepresentations(arrayOf("a")) == 1)
}
