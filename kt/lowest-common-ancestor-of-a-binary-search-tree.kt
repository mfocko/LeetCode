class Solution {
    fun lowestCommonAncestor(
        root: TreeNode?,
        p: TreeNode?,
        q: TreeNode?,
    ): TreeNode? {
        var ancestor: TreeNode? = null
        var pTrack = root
        var qTrack = root

        while (pTrack == qTrack && pTrack != null && qTrack != null) {
            ancestor = pTrack

            if (pTrack!!.`val` == p!!.`val`) {
                return p
            } else if (p!!.`val` < pTrack!!.`val`) {
                pTrack = pTrack.left
            } else {
                pTrack = pTrack.right
            }

            if (qTrack!!.`val` == q!!.`val`) {
                return q
            } else if (q!!.`val` < qTrack!!.`val`) {
                qTrack = qTrack.left
            } else {
                qTrack = qTrack.right
            }
        }

        return ancestor
    }
}
