class Solution {
    fun findChampion(
        n: Int,
        edges: Array<IntArray>,
    ): Int {
        val degrees = IntArray(n)
        for (edge in edges) {
            degrees[edge[1]] += 1
        }

        val candidates =
            degrees.withIndex().filter {
                it.value == 0
            }.take(2)

        return when {
            candidates.size == 1 -> candidates.first().index
            else -> -1
        }
    }
}
