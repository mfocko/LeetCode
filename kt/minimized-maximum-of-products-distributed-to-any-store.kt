class Solution {
    private fun canDistribute(
        n: Int,
        quantities: IntArray,
        pivot: Int,
    ): Boolean {
        var j = 0
        var remaining = quantities[j]

        return (1..n).any {
            if (remaining <= pivot) {
                j += 1
                if (j == quantities.size) {
                    return@any true
                }

                remaining = quantities[j]
            } else {
                remaining -= pivot
            }

            return@any false
        }
    }

    fun minimizedMaximum(
        n: Int,
        quantities: IntArray,
    ): Int {
        var (left, right) = 0 to quantities.max()

        while (left < right) {
            val middle = (left + right) / 2
            if (canDistribute(n, quantities, middle)) {
                right = middle
            } else {
                left = middle + 1
            }
        }

        return left
    }
}
