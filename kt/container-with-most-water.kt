class Solution {
    fun maxArea(height: IntArray): Int {
        var foundMax = 0

        var (l, r) = 0 to height.size - 1
        while (l < r) {
            foundMax =
                listOf(
                    foundMax,
                    (r - l) * listOf(height[l], height[r]).min(),
                ).max()

            when {
                height[l] < height[r] -> l++
                else -> r--
            }
        }

        return foundMax
    }
}
