class Solution {
    fun removeOccurrences(
        s: String,
        part: String,
    ): String =
        s.fold(StringBuilder(s.length)) { res, c ->
            res.append(c)
            if (res.endsWith(part)) {
                res.deleteRange(res.length - part.length, res.length)
            }

            res
        }.toString()
}
