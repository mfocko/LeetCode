class Solution {
    fun isLeaf(node: TreeNode?): Boolean {
        return node != null && node.left == null && node.right == null
    }

    fun removeLeafNodes(
        root: TreeNode?,
        target: Int,
    ): TreeNode? {
        if (root == null) {
            return null
        }

        root.left = removeLeafNodes(root.left, target)
        root.right = removeLeafNodes(root.right, target)

        if (isLeaf(root) && root.`val` == target) {
            return null
        }

        return root
    }
}
