class Solution {
    fun minChanges(s: String): Int =
        s.chunked(2).map {
            if (it[0] != it[1]) 1 else 0
        }.sum()
}
