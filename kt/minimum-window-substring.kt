class Solution {
    fun minWindow(
        s: String,
        t: String,
    ): String {
        val freqs = IntArray(128) { 0 }
        t.forEach {
            freqs[it.toInt()]++
        }

        var (bestLower, bestUpper) = 0 to -1
        var count = t.length
        var (l, r) = 0 to 0

        while (r < s.length) {
            if (freqs[s[r].toInt()]-- > 0) {
                count--
            }

            while (count == 0) {
                if (bestLower >= bestUpper || bestUpper - bestLower + 1 > r - l + 1) {
                    bestLower = l
                    bestUpper = r
                }

                freqs[s[l].toInt()]++

                if (freqs[s[l++].toInt()] > 0) {
                    count++
                }
            }

            r++
        }

        return s.substring(bestLower, bestUpper + 1)
    }
}
