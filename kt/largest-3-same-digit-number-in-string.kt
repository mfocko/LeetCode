class Solution {
    fun largestGoodInteger(num: String): String =
        num
            .asSequence()
            .windowed(3)
            .filter { s ->
                s[0] == s[1] && s[1] == s[2]
            }
            .map { window ->
                window.joinToString("")
            }
            .maxByOrNull { num ->
                num.toInt()
            } ?: ""
}
