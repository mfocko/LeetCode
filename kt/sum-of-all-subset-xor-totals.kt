class Solution {
    private fun xorSum(
        nums: IntArray,
        i: Int,
        sum: Int,
    ): Int {
        if (i == nums.size) {
            return sum
        }

        val including = xorSum(nums, i + 1, sum xor nums[i])
        val excluding = xorSum(nums, i + 1, sum)

        return including + excluding
    }

    fun subsetXORSum(nums: IntArray): Int = xorSum(nums, 0, 0)
}
