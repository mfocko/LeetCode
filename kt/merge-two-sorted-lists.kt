class Solution {
    fun mergeTwoLists(
        list1: ListNode?,
        list2: ListNode?,
    ): ListNode? {
        val head = ListNode(0)
        var (l1, l2) = list1 to list2

        var node = head
        while (l1 != null && l2 != null) {
            if (l1!!.`val` < l2!!.`val`) {
                node.next = l1
                l1 = l1!!.next
            } else {
                node.next = l2
                l2 = l2!!.next
            }

            node = node.next
        }

        l1?.let {
            node.next = it
        }
        l2?.let {
            node.next = it
        }

        return head.next
    }
}
