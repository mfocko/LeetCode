class Solution {
    private data class PartialResult(val last: Character, val circular: Boolean) {
        fun update(word: String): PartialResult = PartialResult(word[word.length - 1], circular && word[0] == last)
    }

    fun isCircularSentence(sentence: String): Boolean =
        sentence.splitToSequence(" ")
            .scan(PartialResult(sentence[sentence.length - 1], true)) { acc, word ->
                acc.update(word)
            }
            .all {
                it.circular
            }
}
