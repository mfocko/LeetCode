class Solution {
    fun checkValid(matrix: Array<IntArray>): Boolean {
        val n = matrix.size
        val required = (1..n).toSet()

        fun checkDimension(dimensions: Iterable<Iterable<Int>>): Boolean = dimensions.all { it.toSet() == required }

        return checkDimension(matrix.map(IntArray::toList)) &&
            checkDimension((0 until n).map { x -> matrix.map { row -> row[x] } })
    }
}
