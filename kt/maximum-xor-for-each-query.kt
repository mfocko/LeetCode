class Solution {
    fun getMaximumXor(
        nums: IntArray,
        maximumBit: Int,
    ): IntArray {
        val mask = 1.shl(maximumBit) - 1

        var runningXor = nums.reduce { x, y -> x.xor(y) }
        return IntArray(nums.size) { i ->
            val current = runningXor.xor(mask)
            runningXor = runningXor.xor(nums[nums.size - 1 - i])
            current
        }
    }
}
