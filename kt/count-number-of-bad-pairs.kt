class Solution {
    fun countBadPairs(nums: IntArray): Long =
        mutableMapOf<Int, Int>().let { counters ->
            nums.indices.sumOf { i ->
                val diff = i - nums[i]
                val goodPairs = counters.get(diff) ?: 0

                counters[diff] = goodPairs + 1

                (i - goodPairs).toLong()
            }
        }
}
