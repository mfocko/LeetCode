class Solution {
    private fun levelOrder(
        root: TreeNode?,
        levels: MutableList<MutableList<Int>>,
        level: Int,
    ): MutableList<MutableList<Int>> {
        if (root == null) {
            return levels
        }

        if (level + 1 > levels.size) {
            levels.add(mutableListOf<Int>())
        }

        levels[level].add(root.`val`)
        levelOrder(root.left, levels, level + 1)
        return levelOrder(root.right, levels, level + 1)
    }

    fun levelOrder(root: TreeNode?): List<List<Int>> = levelOrder(root, mutableListOf<MutableList<Int>>(), 0)
}
