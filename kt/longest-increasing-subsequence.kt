class Solution {
    fun lengthOfLIS(nums: IntArray): Int =
        when (nums.size) {
            0 -> 0
            1 -> 1
            else -> {
                val dp = IntArray(nums.size) { 1 }

                nums.indices
                    .asSequence()
                    .drop(1)
                    .flatMap { i ->
                        (0..i - 1).asSequence().map { j -> i to j }
                    }
                    .maxOf { (i, j) ->
                        when {
                            nums[i] <= nums[j] -> 1
                            else -> {
                                dp[i] = listOf(dp[i], 1 + dp[j]).max()
                                dp[i]
                            }
                        }
                    }
            }
        }
}
