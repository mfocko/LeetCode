class Solution {
    fun tupleSameProduct(nums: IntArray): Int =
        buildMap<Int, Int> {
            nums.indices
                .flatMap { i ->
                    nums.indices.drop(i + 1).map { j ->
                        nums[i] to nums[j]
                    }
                }.forEach { (x, y) ->
                    put(x * y, 1 + (get(x * y) ?: 0))
                }
        }.entries.sumOf { (p, count) ->
            val pairs = (count - 1) * count / 2
            8 * pairs
        }
}
