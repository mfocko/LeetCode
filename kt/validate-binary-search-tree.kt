class Solution {
    private data class SpecialRange(val min: Int? = null, val max: Int? = null) {
        fun check(x: Int): Boolean = (min == null || x > min) && (max == null || x < max)
    }

    private fun isValidBST(
        root: TreeNode?,
        range: SpecialRange,
    ): Boolean =
        when (root) {
            null -> true
            else ->
                range.check(root.`val`) &&
                    isValidBST(root.left, range.copy(max = root.`val`)) &&
                    isValidBST(root.right, range.copy(min = root.`val`))
        }

    fun isValidBST(root: TreeNode?): Boolean = isValidBST(root, SpecialRange())
}
