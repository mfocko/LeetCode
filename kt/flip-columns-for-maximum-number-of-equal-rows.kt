class Solution {
    fun maxEqualRowsAfterFlips(matrix: Array<IntArray>): Int {
        val freqs = mutableMapOf<String, Int>()

        matrix
            .map { row ->
                row.map { it == row[0] }.joinToString(separator = "")
            }
            .forEach { pattern ->
                freqs.put(
                    pattern,
                    1 + freqs.getOrDefault(pattern, 0),
                )
            }

        return freqs.values.max()
    }
}
