class Solution {
    fun rotateTheBox(box: Array<CharArray>): Array<CharArray> {
        val (rows, cols) = box.size to box[0].size
        val rotated = Array(cols) { CharArray(rows) { '.' } }

        (0..<rows).forEach { y ->
            (0..<cols).reversed().fold(cols - 1) { lowestEmpty, x ->
                when (box[y][x]) {
                    '#' -> {
                        rotated[lowestEmpty][rows - y - 1] = '#'
                        lowestEmpty - 1
                    }
                    '*' -> {
                        rotated[x][rows - y - 1] = '*'
                        x - 1
                    }
                    else -> lowestEmpty
                }
            }
        }

        return rotated
    }
}
