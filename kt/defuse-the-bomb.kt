class Solution {
    fun decrypt(
        code: IntArray,
        k: Int,
    ): IntArray {
        val decrypted = IntArray(code.size)
        if (k == 0) {
            return decrypted
        }

        var i = 1
        if (k < 0) {
            i = code.size + k
        }
        val kAbs = listOf(k, -k).max()

        var runningSum = 0
        for (j in i..<i + kAbs) {
            runningSum += code[j % code.size]
        }

        for (j in decrypted.indices) {
            decrypted[j] = runningSum

            runningSum = (
                runningSum -
                    code[i % code.size] +
                    code[(i + kAbs) % code.size]
            )

            i += 1
        }

        return decrypted
    }
}
