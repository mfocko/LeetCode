class Solution {
    private fun get(
        nums: IntArray,
        i: Int,
    ): Int =
        when {
            i < 0 -> 0
            i >= nums.size -> 0
            else -> nums[i]
        }

    fun rob(nums: IntArray): Int =
        when (nums.size) {
            0 -> 0
            else -> {
                nums.indices.drop(1).forEach { i ->
                    nums[i] = listOf(nums[i - 1], get(nums, i - 2) + nums[i]).max()
                }

                nums.last()
            }
        }
}
