class Solution {
    data class BoxType(val boxes: Int, val units: Int)

    fun toBoxType(x: IntArray): BoxType = BoxType(x[0], x[1])

    fun maximumUnits(
        boxTypes: Array<IntArray>,
        truckSize: Int,
    ): Int =
        boxTypes
            .map { toBoxType(it) }
            .sortedByDescending(BoxType::units)
            .fold(0 to 0) { acc, boxType ->
                if (acc.first < truckSize) {
                    val count = minOf(truckSize - acc.first, boxType.boxes)

                    (acc.first + count) to (acc.second + count * boxType.units)
                } else {
                    acc
                }
            }
            .second
}
