class Solution {
    fun clearDigits(s: String): String =
        s.fold(mutableListOf<Char>()) { chars, c ->
            when {
                c.isDigit() -> chars.removeLast()
                else -> chars.add(c)
            }

            chars
        }.joinToString(separator = "")
}
