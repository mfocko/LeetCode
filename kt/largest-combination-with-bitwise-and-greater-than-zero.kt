class Solution {
    private val maxBits = 24

    private data class State(val counters: IntArray) {
        fun update(num: Int): State {
            counters.indices.fold(num) { num, i ->
                if (num % 2 != 0) {
                    counters[i] += 1
                }

                num.shr(1)
            }

            return this
        }
    }

    fun largestCombination(candidates: IntArray): Int =
        candidates.fold(State(IntArray(maxBits))) { acc, it ->
            acc.update(it)
        }.counters.max()
}
