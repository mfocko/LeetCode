class Solution {
    companion object {
        private val MOD: Int = 1_000_000_007
    }

    private data class Acc(val count: Int, val sum: Int, val odd: Int, val even: Int) {
        constructor() : this(0, 0, 0, 1) {}

        fun update(num: Int): Acc =
            (sum + num).let { sum ->
                when {
                    sum % 2 == 0 ->
                        this.copy(
                            count = (count + odd) % MOD,
                            sum = sum,
                            even = even + 1,
                        )
                    else ->
                        this.copy(
                            count = (count + even) % MOD,
                            sum = sum,
                            odd = odd + 1,
                        )
                }
            }
    }

    fun numOfSubarrays(arr: IntArray): Int =
        arr.fold(Acc()) { acc, it ->
            acc.update(it)
        }.count
}
