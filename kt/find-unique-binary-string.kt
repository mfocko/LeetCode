class Solution {
    companion object {
        private val MAPPING: Map<Char, Char> = mapOf('0' to '1', '1' to '0')
    }

    fun findDifferentBinaryString(nums: Array<String>): String =
        nums
            .withIndex()
            .fold(StringBuilder(nums.size)) { sb, it ->
                sb.append(MAPPING[it.value[it.index]]!!)
            }.toString()
}
