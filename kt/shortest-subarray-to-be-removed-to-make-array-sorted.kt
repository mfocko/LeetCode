class Solution {
    fun findLengthOfShortestSubarray(arr: IntArray): Int {
        var (left, right) = 0 to arr.size - 1

        // shift ‹right› while ensorted
        while (right > 0 && arr[right] >= arr[right - 1]) {
            right -= 1
        }

        var shortest = right
        while (left < right && (left < 1 || arr[left - 1] <= arr[left])) {
            while (right < arr.size && arr[left] > arr[right]) {
                right += 1
            }

            shortest = listOf(shortest, right - left - 1).min()
            left += 1
        }

        return shortest
    }
}
