class Solution {
    fun climbStairs(n: Int): Int =
        when {
            n < 3 -> n
            else ->
                (3..n).fold(Triple(0, 1, 2)) { acc, _ ->
                    val (_, y, z) = acc
                    Triple(y, z, y + z)
                }.third
        }
}
