class Solution {
    fun mergeArrays(
        nums1: Array<IntArray>,
        nums2: Array<IntArray>,
    ): Array<IntArray> {
        val (n, m) = nums1.size to nums2.size
        var (i, j) = 0 to 0

        val result: MutableList<IntArray> = mutableListOf()
        while (i < n && j < m) {
            when {
                nums1[i][0] == nums2[j][0] -> {
                    result.add(intArrayOf(nums1[i][0], nums1[i][1] + nums2[j][1]))
                    i++
                    j++
                }
                nums1[i][0] < nums2[j][0] -> {
                    result.add(nums1[i])
                    i++
                }
                else -> {
                    result.add(nums2[j])
                    j++
                }
            }
        }

        while (i < n) {
            result.add(nums1[i])
            i++
        }
        while (j < m) {
            result.add(nums2[j])
            j++
        }

        return result.toTypedArray()
    }
}
