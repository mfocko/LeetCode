class Solution {
    fun isPalindrome(s: String): Boolean =
        s.toCharArray()
            .map { c -> c.toLowerCase() }
            .filter { c -> c.isLetterOrDigit() }
            .toCharArray()
            .let { s ->
                s.indices
                    .map { l -> l to s.size - l - 1 }
                    .takeWhile { (l, r) -> l < r }
                    .all { (l, r) -> s[l] == s[r] }
            }
}
