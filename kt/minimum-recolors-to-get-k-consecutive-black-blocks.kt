class Solution {
    private data class Acc(var l: Int, var white: Int, var recolors: Int) {
        constructor() : this(0, 0, Int.MAX_VALUE)

        fun update(
            blocks: String,
            k: Int,
            c: IndexedValue<Char>,
        ): Acc {
            if (c.value == 'W') {
                white++
            }

            if (c.index - l + 1 == k) {
                recolors = minOf(recolors, white)

                if (blocks[l] == 'W') {
                    white--
                }

                l++
            }

            return this
        }
    }

    fun minimumRecolors(
        blocks: String,
        k: Int,
    ): Int =
        blocks.toCharArray().withIndex().fold(Acc()) { acc, c ->
            acc.update(blocks, k, c)
        }.recolors
}
