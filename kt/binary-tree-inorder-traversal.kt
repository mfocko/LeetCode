class Solution {
    fun inorderTraversal(
        root: TreeNode?,
        values: MutableList<Int>,
    ): List<Int> {
        if (root == null) {
            return values
        }

        inorderTraversal(root.left, values)
        values.add(root.`val`!!)
        inorderTraversal(root.right, values)

        return values
    }

    fun inorderTraversal(root: TreeNode?): List<Int> = inorderTraversal(root, mutableListOf<Int>())
}
