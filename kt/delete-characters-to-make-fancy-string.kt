class Solution {
    fun makeFancyString(s: String): String =
        s.windowedSequence(3, partialWindows = true)
            .filter { it.length < 3 || it[0] != it[1] || it[1] != it[2] }
            .map { it[0] }
            .joinToString(separator = "")
}
