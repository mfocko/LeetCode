class ProductOfNumbers() {
    private val prefix: MutableList<Int> = mutableListOf(1)
    private val size: Int
        get() = prefix.size - 1

    fun add(num: Int) =
        when (num) {
            0 -> {
                prefix.clear()
                prefix.add(1)
            }
            else -> {
                prefix.add(prefix[size] * num)
            }
        }

    fun getProduct(k: Int): Int =
        when {
            k > size -> 0
            else -> prefix[size] / prefix[size - k]
        }
}
