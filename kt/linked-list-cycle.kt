class Solution {
    fun hasCycle(head: ListNode?): Boolean {
        var (x, y) = head to head

        while (y != null && y!!.next != null) {
            x = x!!.next
            y = y!!.next!!.next

            if (x == y) {
                return true
            }
        }

        return false
    }
}
