class Solution {
    fun maximumWealth(accounts: Array<IntArray>): Int =
        accounts.map { account ->
            account.sum()
        }.maxOrNull()!!
}
