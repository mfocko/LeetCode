class Solution {
    fun longestSquareStreak(nums: IntArray): Int {
        nums.sort()

        val foundBest = mutableMapOf<Int, Int>()
        for (num in nums) {
            val squared = num * num
            foundBest.put(squared, 1 + foundBest.getOrDefault(num, 0))
        }

        return foundBest.values.filter {
            it >= 2
        }.maxOrNull() ?: -1
    }
}
