class Solution {
    private fun mapSeats(seats: IntArray): MutableList<Int> = seats.map { if (it == 1) 0 else Int.MAX_VALUE }.toMutableList()

    fun maxDistToClosest(seats: IntArray): Int {
        val left: MutableList<Int> = mapSeats(seats)
        for (i in left.indices.drop(1).filter { left[it] != 0 }) {
            left[i] = left[i - 1] + 1
        }

        val right: MutableList<Int> = mapSeats(seats)
        for (i in right.indices.reversed().drop(1).filter { right[it] != 0 }) {
            right[i] = right[i + 1] + 1
        }

        return left.zip(right).map { (l, r) -> if (l < r) l else r }.max()!!
    }
}
