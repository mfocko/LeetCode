class Solution {
    fun pivotArray(
        nums: IntArray,
        pivot: Int,
    ): IntArray {
        val res = IntArray(nums.size)

        var (smaller, greater) = 0 to nums.size - 1
        nums.indices.map { i -> i to nums.size - 1 - i }.forEach { (i, j) ->
            if (nums[i] < pivot) {
                res[smaller] = nums[i]
                smaller++
            }

            if (nums[j] > pivot) {
                res[greater] = nums[j]
                greater--
            }
        }

        while (smaller <= greater) {
            res[smaller] = pivot
            smaller++
        }

        return res
    }
}
