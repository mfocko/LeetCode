fun <A, B> product(
    xs: Sequence<A>,
    ys: Sequence<B>,
): Sequence<Pair<A, B>> = xs.flatMap { x -> ys.map { y -> x to y } }

fun <A, B> product(
    xs: Iterable<A>,
    ys: Iterable<B>,
): Sequence<Pair<A, B>> = product(xs.asSequence(), ys.asSequence())

class Solution {
    fun runBFS(
        grid: Array<IntArray>,
        coords: Pair<Int, Int>,
    ): Int {
        val queue = ArrayDeque<Pair<Int, Int>>()
        queue.addLast(coords)

        var size = 0
        while (!queue.isEmpty()) {
            val (y, x) = queue.removeFirst()
            if (grid[y][x] != 1) {
                continue
            }

            for ((dy, dx) in sequenceOf(0 to 1, 1 to 0, 0 to -1, -1 to 0)) {
                if (!(y + dy in grid.indices) || !(x + dx in grid[y + dy].indices)) {
                    continue
                } else if (grid[y + dy][x + dx] != 1) {
                    continue
                }

                queue.addLast(y + dy to x + dx)
            }

            // mark it as done
            grid[y][x] = 0
            size++
        }

        return size
    }

    fun maxAreaOfIsland(grid: Array<IntArray>): Int = product(grid.indices, grid.first().indices).map { runBFS(grid, it) }.max() ?: 0
}
