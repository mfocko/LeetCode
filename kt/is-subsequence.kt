class Solution {
    fun isSubsequence(
        s: String,
        t: String,
    ): Boolean {
        var i = 0

        for (j in t.indices) {
            if (i < s.length && t[j] == s[i]) {
                i++
            }

            if (i >= s.length) {
                break
            }
        }

        return i == s.length
    }
}
