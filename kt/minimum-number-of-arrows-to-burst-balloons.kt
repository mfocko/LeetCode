class Solution {
    fun findMinArrowShots(points: Array<IntArray>): Int {
        points.sortBy { it[0] }

        var usedArrows = 1
        points.drop(1).fold(points[0][1]) { maxX, it ->
            when {
                it[0] <= maxX -> listOf(maxX, it[1]).min()
                else -> {
                    usedArrows++

                    it[1]
                }
            }
        }

        return usedArrows
    }
}
