class Solution {
    fun resultsArray(
        nums: IntArray,
        k: Int,
    ): IntArray {
        if (k == 1) {
            return nums
        }

        val result = IntArray(nums.size - k + 1) { -1 }
        (0..<nums.size - 1)
            .scan(1) { consecutive, i ->
                if (nums[i] == nums[i + 1] - 1) {
                    consecutive + 1
                } else {
                    1
                }
            }
            .withIndex()
            .drop(1)
            .filter {
                it.value >= k
            }
            .forEach {
                result[it.index - k + 1] = nums[it.index]
            }
        return result
    }
}
