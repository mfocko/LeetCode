/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class Solution {
    public ListNode ReverseList(ListNode head) {
        ListNode next = null;

        while (head != null) {
            var toRight = head.next;
            head.next = next;
            next = head;
            head = toRight;
        }

        return next;
    }
}
