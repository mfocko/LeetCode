public class Solution {
    private static readonly int MOD = 1_000_000_007;

    public int CountGoodStrings(int low, int high, int zero, int one) {
        var dp = new int[high + 1];
        dp[0] = 1;

        for (var end = 1; end <= high; ++end) {
            if (end >= zero) {
                dp[end] += dp[end - zero];
            }

            if (end >= one) {
                dp[end] += dp[end - one];
            }

            dp[end] %= MOD;
        }

        var good = 0;
        for (var length = low; length <= high; ++length) {
            good += dp[length];
            good %= MOD;
        }

        return good;
    }
}
