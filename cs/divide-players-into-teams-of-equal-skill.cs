public class Solution {
    public long DividePlayers(int[] skill) {
        Array.Sort(skill);

        var expectedSkill = skill[0] + skill[skill.Length - 1];

        long chemistry = 0;
        for (int i = 0, j = skill.Length - 1; i < j; ++i, --j) {
            if (skill[i] + skill[j] != expectedSkill) {
                return -1;
            }

            chemistry += skill[i] * (long)skill[j];
        }

        return chemistry;
    }
}
