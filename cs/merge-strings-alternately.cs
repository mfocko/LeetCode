public class Solution {
    public string MergeAlternately(string word1, string word2) {
        var sb = new StringBuilder();

        int i, j;
        for (i = 0, j = 0; i < word1.Length && j < word2.Length; ++i, ++j) {
            sb.Append(word1[i]);
            sb.Append(word2[j]);
        }

        if (i < word1.Length) {
            sb.Append(word1.Substring(i));
        }
        if (j < word2.Length) {
            sb.Append(word2.Substring(j));
        }

        return sb.ToString();
    }
}
