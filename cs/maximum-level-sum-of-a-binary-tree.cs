/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class Solution {
    private void MaxSum(List<int> levels, int level, TreeNode node) {
        if (node == null) {
            return;
        }

        while (level >= levels.Count) {
            levels.Add(0);
        }
        levels[level] += node.val;

        MaxSum(levels, level + 1, node.left);
        MaxSum(levels, level + 1, node.right);
    }

    public int MaxLevelSum(TreeNode root) {
        var levels = new List<int>();

        MaxSum(levels, 0, root);

        var maxIdx = 0;
        for (var i = 1; i < levels.Count; ++i) {
            if (levels[i] > levels[maxIdx]) {
                maxIdx = i;
            }
        }

        return maxIdx + 1;
    }
}
