public class Solution {
    private int MostSignificantBit(int num) {
        var mostSignificant = 0;
        for (var mask = 1; mask <= num; mask <<= 1) {
            if ((num & mask) != 0) {
                mostSignificant = mask;
            }
        }
        return mostSignificant;
    }

    public int MinimizeXor(int num1, int num2) {
        var bitCount = int.PopCount(num2);
        var mask = MostSignificantBit(num1);

        var minimal = 0;

        // Cancel out upper bits
        for (; mask > 0 && bitCount > 0; mask >>= 1) {
            if ((num1 & mask) != 0) {
                minimal |= mask;
                --bitCount;
            }
        }

        // Add remaining bits from the “bottom”
        for (mask = 1; bitCount > 0; mask <<= 1) {
            if ((minimal & mask) == 0) {
                minimal |= mask;
                --bitCount;
            }
        }

        return minimal;
    }
}
