public class Solution {
    public int MaxProfit(int[] prices, int fee) {
        var profit = 0;
        var buyPrice = prices[0];

        for (var i = 1; i < prices.Length; ++i) {
            profit = Math.Max(profit, prices[i] - buyPrice - fee);
            buyPrice = Math.Min(buyPrice, prices[i] - profit);
        }

        return profit;
    }
}
