public class Solution {
    public bool IsArraySpecial(int[] nums)
        => nums.Zip(nums.Skip(1)).All(p => (p.First & 1) != (p.Second & 1));
}
