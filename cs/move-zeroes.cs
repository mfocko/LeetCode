public class Solution {
    public void MoveZeroes(int[] nums) {
        var i = 0;
        // find first zero
        while (i < nums.Length && nums[i] != 0) {
            ++i;
        }

        for (var j = i + 1; j < nums.Length; ++j) {
            // we do nothing for zeroes
            if (nums[j] == 0) {
                continue;
            }

            // find first zero
            while (i < nums.Length && nums[i] != 0) {
                ++i;
            }

            // no zero has been found
            if (i >= nums.Length) {
                break;
            }

            // swap them while maintaining the order
            nums[i] = nums[j];
            nums[j] = 0;
            ++i;
        }
    }
}
