public class Solution {
    public long CountSubarrays(int[] nums, int minK, int maxK) {
        long count = 0;
        var (minIdx, maxIdx) = (-1, -1);

        for (int i = 0, j = 0; j < nums.Length; ++j) {
            var x = nums[j];

            // is not bounded
            if (x < minK || x > maxK) {
                i = j + 1;
                continue;
            }

            if (x == minK) {
                minIdx = j;
            }

            if (x == maxK) {
                maxIdx = j;
            }

            count += Math.Max(0, Math.Min(minIdx, maxIdx) - i + 1);
        }

        return count;
    }
}
