public class Solution {
    public bool Check(int[] nums) => Enumerable
        .Range(0, nums.Length)
        .Count(i => nums[i] > nums[(i + 1) % nums.Length]) < 2;
}
