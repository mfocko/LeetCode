public class Solution {
    public int[] ArrayRankTransform(int[] arr) {
        var unique = new SortedSet<int>();
        foreach (var x in arr) {
            unique.Add(x);
        }

        var ranks = unique.Select((item, index) => (item, index + 1)).ToDictionary();
        for (int i = 0; i < arr.Length; ++i) {
            arr[i] = ranks[arr[i]];
        }
        return arr;
    }
}
