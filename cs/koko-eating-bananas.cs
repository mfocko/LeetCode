public class Solution {
    public int MinEatingSpeed(int[] piles, int h) {
        double getTotal(int hourly) => piles.Sum(p => double.Ceiling(p / (double)hourly));

        int answer = -1;

        int low = 1, high = piles.Max();
        while (low <= high) {
            var mid = (low + high) / 2;
            var total = getTotal(mid);

            if (total <= h) {
                answer = mid;
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }

        return answer;
    }
}
