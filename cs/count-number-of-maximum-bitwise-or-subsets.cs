public class Solution {
    public int CountMaxOrSubsets(int[] nums) {
        var dp = new int[1 << 17];
        dp[0] = 1;

        var max = 0;
        foreach (var num in nums) {
            for (int i = max; i >= 0; --i) {
                dp[i | num] += dp[i];
            }
            max |= num;
        }

        return dp[max];
    }
}
