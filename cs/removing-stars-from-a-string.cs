public class Solution {
    public string RemoveStars(string s) {
        var sb = new StringBuilder();

        foreach (var c in s) {
            switch (c) {
                case '*':
                    sb.Remove(sb.Length - 1, 1);
                    break;
                default:
                    sb.Append(c);
                    break;
            }
        }

        return sb.ToString();
    }
}
