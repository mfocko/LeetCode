public class Solution {
    private struct Direction {
        int Balls;
        int Moves;

        public Direction() {
            Balls = 0;
            Moves = 0;
        }

        public int Update(char state) {
            var moves = Moves;

            Balls += state - '0';
            Moves += Balls;

            return moves;
        }
    }

    public int[] MinOperations(string boxes) {
        var answer = new int[boxes.Length];

        var (left, right) = (new Direction(), new Direction());
        for (var i = 0; i < boxes.Length; ++i) {
            // from left side
            answer[i] += left.Update(boxes[i]);

            // from right side
            var j = boxes.Length - i - 1;
            answer[j] += right.Update(boxes[j]);
        }

        return answer;
    }
}
