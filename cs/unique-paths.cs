public class Solution {
    private static int Right(List<int> row, int size, int idx) {
        if (idx >= size) {
            return 0;
        }

        return row[idx];
    }

    public int UniquePaths(int m, int n) {
        var bottom = new List<int>(Enumerable.Repeat(1, n));

        for (var y = m - 2; y >= 0; --y) {
            var above = new List<int>(Enumerable.Repeat(0, n));

            for (var x = n - 1; x >= 0; --x) {
                above[x] = Right(above, n, x + 1) + bottom[x];
            }

            bottom = above;
        }

        return bottom[0];
    }
}
