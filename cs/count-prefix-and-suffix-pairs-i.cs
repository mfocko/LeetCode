public class Solution {
    private bool IsPrefixAndSuffix(string str1, string str2) =>
        str1.Length <= str2.Length && str2.StartsWith(str1) && str2.EndsWith(str1);

    public int CountPrefixSuffixPairs(string[] words) {
        var count = 0;

        for (var i = 0; i < words.Length; ++i) {
            for (var j = i + 1; j < words.Length; ++j) {
                if (IsPrefixAndSuffix(words[i], words[j])) {
                    ++count;
                }
            }
        }

        return count;
    }
}
