using System.Linq;

public class Solution {
    public int FindTargetSumWays(int[] nums, int target) {
        var total = nums.Sum();

        var dp = new int[2 * total + 1];
        dp[nums[0] + total] = 1;
        dp[-nums[0] + total] += 1;

        for (int i = 1; i < nums.Length; ++i) {
            var next = new int[2 * total + 1];

            for (int sum = -total; sum <= total; ++sum) {
                if (dp[sum + total] <= 0) {
                    continue;
                }

                next[sum + nums[i] + total] += dp[sum + total];
                next[sum - nums[i] + total] += dp[sum + total];
            }

            dp = next;
        }

        return Math.Abs(target) > total ? 0 : dp[target + total];
    }
}
