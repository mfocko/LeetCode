public class Solution {
    public IList<int> LexicalOrder(int n) {
        var numbers = new List<int>();

        var current = 1;
        for (int i = 0; i < n; ++i) {
            numbers.Add(current);

            if (current * 10 <= n) {
                current *= 10;
                continue;
            }

            // backtrack and try next
            while (current >= n || current % 10 == 9) {
                current /= 10;
            }
            ++current;
        }

        return numbers;
    }
}
