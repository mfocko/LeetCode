public class Solution {
    public int LengthOfLastWord(string s) {
        var lastLength = 0;

        var length = 0;
        foreach (var c in s) {
            if (char.IsWhiteSpace(c)) {
                if (length != 0) {
                    lastLength = length;
                }

                length = 0;
                continue;
            }

            ++length;
        }

        if (length != 0) {
            lastLength = length;
        }

        return lastLength;
    }
}
