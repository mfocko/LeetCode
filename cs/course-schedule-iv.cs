public class Solution {
    public IList<bool> CheckIfPrerequisite(int numCourses, int[][] prerequisites, int[][] queries) {
        var isPrerequisite = new bool[numCourses][];
        for (var i = 0; i < numCourses; ++i) {
            isPrerequisite[i] = new bool[numCourses];
        }

        foreach (var edge in prerequisites) {
            var (u, v) = (edge[0], edge[1]);
            isPrerequisite[u][v] = true;
        }

        for (var v = 0; v < numCourses; ++v) {
            for (var u = 0; u < numCourses; ++u) {
                for (var w = 0; w < numCourses; ++w) {
                    isPrerequisite[u][w] = (isPrerequisite[u][w] || (isPrerequisite[u][v] && isPrerequisite[v][w]));
                }
            }
        }

        var answer = new List<bool>();
        foreach (var query in queries) {
            var (u, v) = (query[0], query[1]);
            answer.Add(isPrerequisite[u][v]);
        }

        return answer;
    }
}
