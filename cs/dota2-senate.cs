public class Solution {
    public string PredictPartyVictory(string senate) {
        var radiant = new Queue<int>();
        var dire = new Queue<int>();

        for (var i = 0; i < senate.Length; ++i) {
            (senate[i] == 'R' ? radiant : dire).Enqueue(i);
        }

        while (radiant.Count > 0 && dire.Count > 0) {
            var r = radiant.Dequeue();
            var d = dire.Dequeue();

            if (r < d) {
                radiant.Enqueue(senate.Length + r);
            } else {
                dire.Enqueue(senate.Length + d);
            }
        }

        if (radiant.Count > 0) {
            return "Radiant";
        }

        return "Dire";
    }
}
