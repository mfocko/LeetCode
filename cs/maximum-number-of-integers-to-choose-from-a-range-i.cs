public class Solution {
    public int MaxCount(int[] banned, int n, int maxSum) {
        Array.Sort(banned);

        int idx = 0, used = 0;
        for (int i = 1; i <= n && maxSum > 0; ++i) {
            var skip = idx < banned.Length && banned[idx] == i;
            while (idx < banned.Length && banned[idx] == i) {
                ++idx;
            }

            if (!skip && maxSum - i >= 0) {
                maxSum -= i;
                ++used;
            }
        }

        return used;
    }
}
