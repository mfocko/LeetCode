public class Solution {
    public bool CanPlaceFlowers(int[] flowerbed, int n) {
        var count = 0;

        int left = 0, right;
        for (var i = 0; i < flowerbed.Length; ++i) {
            right = (i + 1 < flowerbed.Length) ? flowerbed[i + 1] : 0;

            if (left == 0 && flowerbed[i] == 0 && right == 0) {
                ++count;
                left = 1;
            } else {
                left = flowerbed[i];
            }
        }

        return count >= n;
    }
}
