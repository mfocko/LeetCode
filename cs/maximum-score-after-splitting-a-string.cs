public class Solution {
    public int MaxScore(string s) {
        var ones = s.Skip(1).Count(c => c == '1');
        var zeros = s.Take(1).Count(c => c == '0');

        var foundScore = ones + zeros;
        for (var i = 1; i < s.Length - 1; ++i) {
            switch (s[i]) {
                case '0':
                    ++zeros;
                    break;
                case '1':
                    --ones;
                    break;
            }

            foundScore = Math.Max(foundScore, ones + zeros);
        }

        return foundScore;
    }
}
