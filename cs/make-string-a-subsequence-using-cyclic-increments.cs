public class Solution {
    public bool CanMakeSubsequence(string str1, string str2) {
        int j = 0;
        for (int i = 0; i < str1.Length && j < str2.Length; ++i) {
            if (str1[i] == str2[j] || str1[i] + 1 == str2[j] || str1[i] - 25 == str2[j]) {
                ++j;
            }
        }

        return j == str2.Length;
    }
}
