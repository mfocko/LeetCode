public class Solution {
    public enum Direction {
        Left,
        Right,
    }

    private static Direction Next(Direction d) => d switch {
        Direction.Left => Direction.Right,
        Direction.Right => Direction.Left,
        _ => throw new ArgumentException("Invalid direction")
    };

    private static TreeNode? Get(Direction d, TreeNode n) => d switch {
        Direction.Left => n.left,
        Direction.Right => n.right,
        _ => throw new ArgumentException("Invalid direction")
    };

    private int LongestZigZag(TreeNode node, int steps, Direction direction) {
        if (node == null) {
            return steps;
        }

        // Current path
        var maxSteps = steps;

        // Switching direction
        maxSteps = Math.Max(maxSteps, LongestZigZag(Get(direction, node), steps + 1, Next(direction)));

        // Starting new path
        maxSteps = Math.Max(maxSteps, LongestZigZag(Get(Next(direction), node), 0, direction));

        return maxSteps;
    }

    public int LongestZigZag(TreeNode root)
        => Math.Max(
            LongestZigZag(root, -1, Direction.Left),
            LongestZigZag(root, -1, Direction.Right)
        );
}
