public class Solution {
    public int MinFlips(int a, int b, int c) {
        var flips = 0;

        for (; (a | b) != c; a >>= 1, b >>= 1, c >>= 1) {
            var (aa, bb, cc) = (a & 1, b & 1, c & 1);

            if ((aa | bb) == cc) {
                continue;
            }

            if (aa != 0 && bb != 0) {
                flips += 2;
            } else {
                ++flips;
            }
        }

        return flips;
    }
}
