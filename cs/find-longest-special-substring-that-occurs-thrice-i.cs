public class Solution {
    public int MaximumLength(string s) {
        var counter = new Dictionary<(char, int), int>();

        for (var l = 0; l < s.Length; ++l) {
            var c = s[l];

            var substringLength = 0;
            for (var r = l; r < s.Length && c == s[r]; ++r) {
                ++substringLength;

                var key = (c, substringLength);
                counter[key] = 1 + (counter.ContainsKey(key) ? counter[key] : 0);
            }
        }

        return counter.Select(pair => {
            if (pair.Value < 3) {
                return null;
            }

            var (c, length) = pair.Key;
            return (int?)length;
        }).Max() ?? -1;
    }
}
