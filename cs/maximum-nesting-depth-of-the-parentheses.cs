public class Solution {
    public int MaxDepth(string s) {
        int maxDepth = 0;

        int open = 0;
        foreach (var c in s) {
            switch (c) {
                case '(':
                    ++open;
                    break;
                case ')':
                    --open;
                    break;
            }

            maxDepth = Math.Max(maxDepth, open);
        }

        return maxDepth;
    }
}
