public class Solution {
    public int PivotIndex(int[] nums) {
        var fromLeft = 0;
        var fromRight = nums.Sum();

        for (var i = 0; i < nums.Length; ++i) {
            fromRight -= nums[i];

            if (fromLeft == fromRight) {
                return i;
            }

            fromLeft += nums[i];
        }

        return -1;
    }
}
