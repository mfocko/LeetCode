public class Solution {
    public int Tribonacci(int n) {
        var sequence = new int[] { 0, 1, 1 };

        if (n < 3) {
            return sequence[n];
        }

        for (var i = 3; i <= n; ++i) {
            var next = sequence.Sum();
            sequence[0] = sequence[1];
            sequence[1] = sequence[2];
            sequence[2] = next;
        }

        return sequence[2];
    }
}
