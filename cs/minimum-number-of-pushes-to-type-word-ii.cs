public class Solution {
    private int[] GetFreqs(string word) {
        var freqs = new int[26];
        foreach (char c in word) {
            ++freqs[c - 'a'];
        }
        return freqs;
    }

    public int MinimumPushes(string word) {
        // Count occurences of letters
        var freqs = GetFreqs(word);

        // Sort them (letters don't matter, only counts)
        Array.Sort(freqs);

        var pushes = 0;

        var (minPushes, used) = (1, 0);
        for (var i = freqs.Length - 1; i >= 0 && freqs[i] > 0; --i) {
            pushes += freqs[i] * minPushes;
            ++used;

            // used all buttons for the current position
            if (used >= 8) {
                ++minPushes;
                used = 0;
            }
        }

        return pushes;
    }
}
