/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class Solution {
    private int GoodNodes(TreeNode root, int m) {
        if (root == null) {
            return 0;
        }

        int newMax = Math.Max(m, root.val);
        return (root.val >= m ? 1 : 0) + GoodNodes(root.left, newMax) + GoodNodes(root.right, newMax);
    }

    public int GoodNodes(TreeNode root) => GoodNodes(root, int.MinValue);
}
