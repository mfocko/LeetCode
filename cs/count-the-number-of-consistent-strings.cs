public class Solution {
    private static int GetMask(string allowed) {
        var mask = 0;

        foreach (var c in allowed) {
            mask |= 1 << (c - 'a');
        }

        return mask;
    }

    private static bool IsConsistent(int mask, string word)
        => word.All(c => (mask & (1 << (c - 'a'))) != 0);

    public int CountConsistentStrings(string allowed, string[] words) {
        var mask = GetMask(allowed);
        return words.Count(word => IsConsistent(mask, word));
    }
}
