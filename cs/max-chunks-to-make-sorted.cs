public class Solution {
    public int MaxChunksToSorted(int[] arr) {
        var chunks = 0;

        var maximum = arr[0];
        for (int i = 0; i < arr.Length; ++i) {
            maximum = Math.Max(maximum, arr[i]);

            if (maximum == i) {
                ++chunks;
            }
        }

        return chunks;
    }
}
