public class Solution {
    public int MinBitFlips(int start, int goal) => int.PopCount(start ^ goal);
}
