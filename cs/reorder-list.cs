using System.Collections.Generic;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class Solution {
    private List<ListNode> ListAsList(ListNode head) {
        var result = new List<ListNode>();

        for (var node = head; node != null; node = node.next) {
            result.Add(node);
        }

        return result;
    }

    public void ReorderList(ListNode head) {
        var nodes = ListAsList(head);

        ListNode node = head;
        for (int i = 1, j = nodes.Count - 1; i <= j; ++i, --j) {
            node.next = nodes[j];
            node = node.next;

            node.next = nodes[i];
            node = node.next;
        }
        node.next = null;
    }
}
