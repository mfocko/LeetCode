public class Solution {
    public int BitToInt(int num) => num == 0 ? 1 : 0;

    public int LongestSubarray(int[] nums) {
        var longest = 0;

        var zeros = 0;
        for (int l = 0, r = 0; r < nums.Length; ++r) {
            zeros += BitToInt(nums[r]);

            for (; zeros > 1; ++l) {
                zeros -= BitToInt(nums[l]);
            }

            longest = Math.Max(longest, r - l);
        }

        return longest;
    }
}
