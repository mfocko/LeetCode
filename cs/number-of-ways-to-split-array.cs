public class Solution {
    public int WaysToSplitArray(int[] nums) {
        var (leftSum, rightSum) = (0l, nums.Select(x => (long)x).Sum());

        var count = 0;
        for (var i = 0; i < nums.Length - 1; ++i) {
            leftSum += nums[i];
            rightSum -= nums[i];

            if (leftSum >= rightSum) {
                ++count;
            }
        }

        return count;
    }
}
