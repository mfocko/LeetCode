public class Solution {
    public int[] MissingRolls(int[] rolls, int mean, int n) {
        var sum = rolls.Sum();

        var remainder = (rolls.Length + n) * mean - sum;
        if (remainder < n || remainder > 6 * n) {
            // cannot construct such rolls
            return [];
        }

        var (roll, unmatched) = (remainder / n, remainder % n);

        var missing = new int[n];
        Array.Fill(missing, roll);

        for (int i = 0; i < unmatched; ++i) {
            ++missing[i];
        }

        return missing;
    }
}
