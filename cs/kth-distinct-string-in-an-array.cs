public class Solution {
    public string KthDistinct(string[] arr, int k) {
        var uniqueWords = arr.ToHashSet();
        var freqs = uniqueWords.ToDictionary(key => key, key => arr.Count(w => w == key));

        var (i, found) = (0, 0);
        for (; i < arr.Length && found < k; ++i) {
            if (freqs[arr[i]] != 1) {
                continue;
            }

            ++found;
        }

        if (found < k) {
            return "";
        }

        return arr[i - 1];
    }
}
