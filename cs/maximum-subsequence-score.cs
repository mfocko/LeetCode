using System.Collections.Generic;

public class Solution {
    public long MaxScore(int[] nums1, int[] nums2, int k) {
        long prefixSum = 0, maxScore = 0;

        var heap = new PriorityQueue<int, int>();
        foreach (var (num, mult) in nums1.Zip(nums2).OrderBy(key => key.Second).Reverse()) {
            prefixSum += num;
            heap.Enqueue(num, num);

            if (heap.Count == k) {
                maxScore = Math.Max(maxScore, prefixSum * mult);
                prefixSum -= heap.Dequeue();
            }
        }

        return maxScore;
    }
}
