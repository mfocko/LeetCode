public class Solution {
    public string MakeGood(string s) {
        var st = new List<char>();

        foreach (char c in s) {
            var idx = st.Count - 1;

            if (
                idx < 0
                || Char.ToLower(c) != Char.ToLower(st[idx])
                || Char.IsUpper(c) == Char.IsUpper(st[idx])
            ) {
                st.Add(c);
            } else {
                st.RemoveAt(idx);
            }
        }

        return new string(st.ToArray());
    }
}
