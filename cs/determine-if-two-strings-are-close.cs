public class Solution {
    private static List<int> Freqs(string word) {
        var f = new List<int>(Enumerable.Repeat(0, 26));

        foreach (var c in word) {
            ++f[c - 'a'];
        }

        return f;
    }

    private static int Mask(List<int> f) {
        var m = 0;

        foreach (var c in f) {
            m = (m << 1) | (c > 0 ? 1 : 0);
        }

        return m;
    }

    public bool CloseStrings(string word1, string word2) {
        var f1 = Freqs(word1);
        var m1 = Mask(f1);
        f1.Sort();

        var f2 = Freqs(word2);
        var m2 = Mask(f2);
        f2.Sort();

        return m1 == m2 && f1.SequenceEqual(f2);
    }
}
