public class Solution {
    public int[] FindThePrefixCommonArray(int[] xs, int[] ys) {
        var n = xs.Length;
        var freqs = new int[n + 1];

        var (common, result) = (0, new int[n]);
        for (var i = 0; i < n; ++i) {
            foreach (var x in new List<int>() { xs[i], ys[i] }) {
                ++freqs[x];
                if (freqs[x] == 2) {
                    // occurred in both
                    ++common;
                }
            }

            result[i] = common;
        }

        return result;
    }
}
