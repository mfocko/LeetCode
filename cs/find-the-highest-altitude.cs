public class Solution {
    public int LargestAltitude(int[] gain) {
        int m = 0;
        int alt = 0;

        foreach (var d in gain) {
            alt += d;
            m = Math.Max(m, alt);
        }

        return m;
    }
}
