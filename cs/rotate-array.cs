public class Solution {
    public void Rotate(int[] nums, int k) {
        void Reverse(int begin, int end) {
            for (/* no-op */; begin < end; ++begin, --end) {
                (nums[begin], nums[end]) = (nums[end], nums[begin]);
            }
        }

        k %= nums.Length;

        Reverse(0, nums.Length - 1);
        Reverse(0, k - 1);
        Reverse(k, nums.Length - 1);
    }
}
