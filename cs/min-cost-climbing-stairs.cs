public class Solution {
    public int MinCostClimbingStairs(int[] cost) {
        int Get(int k) {
            if (k < 0 || k >= cost.Length) {
                return 0;
            }

            return cost[k];
        }

        for (var i = cost.Length - 1; i >= 0; --i) {
            cost[i] += Math.Min(Get(i + 1), Get(i + 2));
        }

        return Math.Min(cost[0], cost[1]);
    }
}
