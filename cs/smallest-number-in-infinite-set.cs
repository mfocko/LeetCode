public class SmallestInfiniteSet {
    private SortedSet<int> smaller;
    private int nextInfinite;

    public SmallestInfiniteSet() {
        smaller = new SortedSet<int>();
        nextInfinite = 1;
    }

    public int PopSmallest() {
        var m = smaller.Min;
        if (m != 0) {
            smaller.Remove(m);
            return m;
        }

        ++nextInfinite;
        return nextInfinite - 1;
    }

    public void AddBack(int num) {
        if (num >= nextInfinite) {
            return;
        }

        smaller.Add(num);
    }
}

/**
 * Your SmallestInfiniteSet object will be instantiated and called as such:
 * SmallestInfiniteSet obj = new SmallestInfiniteSet();
 * int param_1 = obj.PopSmallest();
 * obj.AddBack(num);
 */
