public class Solution {
    public int[] CountBits(int n) {
        var counters = new int[n + 1];

        for (var i = 1; i <= n; ++i) {
            counters[i] = counters[i >> 1] + i % 2;
        }

        return counters;
    }
}
