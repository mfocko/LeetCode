using System.Collections.Generic;

public class Solution {
    public ListNode ModifiedList(int[] nums, ListNode head) {
        var uniqueNums = new HashSet<int>(nums);

        var node = head;
        while (node.next != null) {
            if (uniqueNums.Contains(node.next.val)) {
                node.next = node.next.next;
            } else {
                node = node.next;
            }
        }

        if (uniqueNums.Contains(head.val)) {
            return head.next;
        }
        return head;
    }
}
