/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class Solution {
    private static bool PruneTreeRec(TreeNode? node) {
        if (node == null) {
            return false;
        }

        if (!PruneTreeRec(node.left)) {
            node.left = null;
        }
        if (!PruneTreeRec(node.right)) {
            node.right = null;
        }

        return node.val == 1 || node.left != null || node.right != null;
    }

    public TreeNode? PruneTree(TreeNode? root) {
        if (!PruneTreeRec(root)) {
            return null;
        }
        return root;
    }
}
