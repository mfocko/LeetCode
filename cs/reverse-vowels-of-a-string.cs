public class Solution {
    private bool IsVowel(char c) {
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
    }

    public string ReverseVowels(string s) {
        var reversed = new StringBuilder(s);

        s = s.ToLower();
        for (int l = 0, r = reversed.Length - 1; l < r; ++l, --r) {
            while (l < reversed.Length && !IsVowel(s[l])) {
                ++l;
            }

            while (r > 0 && !IsVowel(s[r])) {
                --r;
            }

            if (l < r) {
                (reversed[l], reversed[r]) = (reversed[r], reversed[l]);
            }
        }

        return reversed.ToString();
    }
}
