public class Solution {
    public int LongestOnes(int[] nums, int k) {
        int maxLength = 0;

        int zeros = 0;
        for (int i = 0, j = 0; j < nums.Length; ++j) {
            if (nums[j] == 0) {
                ++zeros;
            }

            for (; zeros > k; ++i) {
                if (nums[i] == 0) {
                    --zeros;
                }
            }

            maxLength = Math.Max(maxLength, j - i + 1);
        }

        return maxLength;
    }
}
