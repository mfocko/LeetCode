public class Solution {
    private bool InBounds(int[][] mat, int x, int y)
        => y >= 0 && y < mat.Length && x >= 0 && x < mat[y].Length;

    private int Check(int[][] mat, Dictionary<int, int> indices, (int, int) start, (int, int) d) {
        var lastToPaint = int.MinValue;

        var (dx, dy) = d;
        for (var (x, y) = start; InBounds(mat, x, y); x += dx, y += dy) {
            lastToPaint = Math.Max(lastToPaint, indices[mat[y][x]]);
        }

        return lastToPaint;
    }

    public int FirstCompleteIndex(int[] arr, int[][] mat) {
        var indices = new Dictionary<int, int>();
        for (var i = 0; i < arr.Length; ++i) {
            indices[arr[i]] = i;
        }

        var first = int.MaxValue;

        // Check through rows
        for (var y = 0; y < mat.Length; ++y) {
            first = Math.Min(first, Check(mat, indices, (0, y), (1, 0)));
        }

        // Check through columns
        for (var x = 0; x < mat[0].Length; ++x) {
            first = Math.Min(first, Check(mat, indices, (x, 0), (0, 1)));
        }

        return first;
    }
}
