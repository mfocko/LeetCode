public class Solution {
    public bool CheckValidString(string s) {
        int left = 0;
        int right = 0;

        for (int i = 0, j = s.Length - 1; i < s.Length; ++i, --j) {
            if (s[i] == '(' || s[i] == '*') {
                ++left;
            } else {
                --left;
            }

            if (s[j] == ')' || s[j] == '*') {
                ++right;
            } else {
                --right;
            }

            if (left < 0 || right < 0) {
                return false;
            }
        }

        return true;
    }
}
