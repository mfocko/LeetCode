public class Solution {
    public int EqualPairs(int[][] grid) {
        var count = 0;
        var n = grid.Length;

        // Check each row ‹r› against each column ‹c›.
        for (var r = 0; r < n; ++r) {
            for (var c = 0; c < n; ++c) {
                var match = true;

                for (var i = 0; i < n; ++i) {
                    if (grid[r][i] != grid[i][c]) {
                        match = false;
                        break;
                    }
                }

                count += match ? 1 : 0;
            }
        }

        return count;
    }
}
