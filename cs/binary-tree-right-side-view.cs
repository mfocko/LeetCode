/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class Solution {
    private void GetView(List<int> levels, int level, TreeNode node) {
        if (node == null) {
            return;
        }

        while (level >= levels.Count) {
            levels.Add(0);
        }
        levels[level] = node.val;

        GetView(levels, level + 1, node.left);
        GetView(levels, level + 1, node.right);
    }

    public IList<int> RightSideView(TreeNode root) {
        var levels = new List<int>();
        GetView(levels, 0, root);
        return levels;
    }
}
