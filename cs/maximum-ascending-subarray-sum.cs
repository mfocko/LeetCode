public class Solution {
    private record MaxAscendingAcc(int Sum, int Max) {
        public MaxAscendingAcc Add(int num) => new(Sum + num, Math.Max(Max, Sum + num));
        public MaxAscendingAcc Reset(int num) => new(num, Math.Max(Max, num));
    }

    public int MaxAscendingSum(int[] nums) =>
        nums.Zip(nums.Skip(1)).Aggregate(
            new MaxAscendingAcc(nums[0], nums[0]),
            (acc, pair) => pair switch {
                _ when pair.First < pair.Second => acc.Add(pair.Second),
                _ => acc.Reset(pair.Second),
            }
        ).Max;
}
