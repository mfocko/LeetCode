public class Solution {
    public int[] SuccessfulPairs(int[] spells, int[] potions, long success) {
        int getSuccessful(long strength) {
            var (left, right) = (0, potions.Length);

            while (left < right) {
                var mid = (left + right) / 2;
                if (strength * potions[mid] >= success) {
                    right = mid;
                } else {
                    left = mid + 1;
                }
            }

            return potions.Length - left;
        }

        // Ensort potions to allow bisection.
        Array.Sort(potions);

        return spells
            .Select(s => getSuccessful(s))
            .ToArray();
    }
}
